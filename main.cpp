/* This file is part of "modern computer flatboat", a pdf viewer.
 * Copyright (C) 2012,2013 Frank Fuhlbrück 
 * License: GPLv3 (or any later version, at your option)
 * See the file "LICENSE".
 */

#include <QVariant>
#if QT_VERSION < 0x050000
#include <QtGui/QApplication>
#else
#include <QtWidgets/QApplication>
#endif

#include "qmlapplicationviewer.h"
#include "qpdfimage.h"
#include "sethist.h"
#include "qappintentactivity.h"
#include <QtGui/QImage>
#include <QtGui/QPixmap>

#if QT_VERSION < 0x050000 
//#include <QDeclarativeEngine>
#include <QDeclarativeContext>
#include <QtGui/QLabel>
#define SET_TITLE setWindowTitle
#define SET_ICON setWindowIcon
#else
#include <QtWidgets/QLabel>
//#include <QQmlEngine>
#include <QQmlContext>
#define SET_TITLE setTitle
#define SET_ICON setIcon
#endif



#include <QDir>
#include <QSettings>
#include <QDebug>
#include <QVariant>

SetHist *sethist;

#ifdef Q_WS_MAEMO_5
#include <QtGui/QX11Info>
#include <X11/Xlib.h>
#include <X11/Xatom.h>
inline void grabVolKeys(WId wid){
	if (!wid)
		return;
	
	Atom atom = XInternAtom(QX11Info::display(), "_HILDON_ZOOM_KEY_ATOM", False);
	if (!atom)
		return;
	
	unsigned long data = 1;
	XChangeProperty (QX11Info::display(),wid,atom,
		XA_INTEGER,32,PropModeReplace,
		reinterpret_cast<unsigned char *>(&data),1);
}
#endif //Q_WS_MAEMO_5

#ifdef Q_OS_ANDROID
#include <jni.h>
#define JNOTOKRETURN(WHAT) if(!WHAT || jenv->ExceptionCheck()){SetHist::log("WHAT went wrong");jenv->ExceptionClear();return JNI_VERSION_1_4;}
JavaVM* jvm = NULL;
jobject qtactivity;


static JNINativeMethod methods[] = {
    {"passViewIntent", "(Ljava/lang/String;)V", (void*)passViewIntent}
};


Q_DECL_EXPORT jint JNICALL JNI_OnLoad(JavaVM* vm, void*){
	jvm = vm;
	JNIEnv *jenv;
	jvm->AttachCurrentThread(&jenv, NULL);
	jclass qtactivity_cls = jenv->FindClass("org/kde/necessitas/origo/QtActivity");
	JNOTOKRETURN(qtactivity_cls)

        jmethodID qtactivity_ctr = jenv->GetMethodID(qtactivity_cls, "<init>", "()V");
	JNOTOKRETURN(qtactivity_ctr)

        jobject loc = jenv->NewObject(qtactivity_cls, qtactivity_ctr);
	JNOTOKRETURN(loc)
        qtactivity = jenv->NewGlobalRef(loc);
	JNOTOKRETURN(qtactivity)
	
	// register native methods
	if (jenv->RegisterNatives(qtactivity_cls, methods, sizeof(methods) / sizeof(methods[0])) < 0){
        	qCritical()<<"RegisterNatives failed";
        	return -1;
	}

	return JNI_VERSION_1_4;
}


#endif

Q_DECL_EXPORT int main(int argc, char *argv[]){
	qmlRegisterType<QPdfImage>("flatboat.PdfImage", 1, 0, "PdfImage");
	
	QScopedPointer<QAppIntentActivity> app(new QAppIntentActivity(argc, argv));
	QScopedPointer<QmlApplicationViewer> viewer(QmlApplicationViewer::create());
	app->setOrganizationName("flatboat");
	app->setApplicationName("flatboat");
	
// #ifdef Q_OS_ANDROID
// 	QFont stdfont = app->font(); 
// 	stdfont.setPointSize(12);
// 	app->setFont(stdfont);
// #endif //Q_OS_ANDROID

#ifdef Q_OS_MAC
	QDir dir(QApplication::applicationDirPath());
	dir.cdUp();
	dir.cd("plugins");
	QApplication::setLibraryPaths(QStringList(dir.absolutePath()));
#endif
	
	sethist = new SetHist();


#ifndef Q_OS_ANDROID
	//since necessitas alpha 4 we get an argument
	//TODO:figure out what it contains
	QStringList args = app->arguments();
	if(args.length() >= 2){
		QString fn = QUrl(args[1]).scheme() == "file" ? QUrl(args[1]).toLocalFile() : args[1];
		QVariantMap doc;
		doc["filepath"] = fn;
		sethist->newDocument(doc);
	}
#endif //NOT Q_OS_ANDROID
	
	
	viewer->setOrientation(QmlApplicationViewer::ScreenOrientationAuto);
	viewer->rootContext()->setContextProperty("sethist", sethist);
	viewer->setSource(QUrl(QLatin1String("qrc:/main.qml")));
	
	#if QT_VERSION < 0x050000
	viewer->setUpdatesEnabled(true);
	#endif
	
	QObject::connect((*viewer).rootObject(), SIGNAL(showFullScreen()),
			 viewer.data(), SLOT(showFullScreen()));
	QObject::connect((*viewer).rootObject(), SIGNAL(showNormal()),
			 viewer.data(), SLOT(showNormal()));
	QObject::connect(app.data(), SIGNAL(fileOpen(QString)),
			 (*viewer).rootObject(), SIGNAL(fileOpen(QString)));
	QObject::connect((*viewer).rootObject(), SIGNAL(windowTitleChangeRequest(QString)),
			 viewer.data(), 
	#if QT_VERSION < 0x050000
		SLOT(setWindowTitle(QString)));
	#else
		SLOT(setTitle(QString)));
	#endif
	
	//alternative solution: set an allConnected property or similar
	viewer->SET_TITLE((*viewer).rootObject()->property("windowTitle").toString());
	viewer->showExpanded();
	viewer->SET_ICON(QPixmap::fromImage(QImage(":/flatboat.png")).scaled(32,32));
	
	sethist->setWindow(viewer.data());
	
#ifdef Q_WS_MAEMO_5
	grabVolKeys(viewer->winId());
#endif //Q_WS_MAEMO_5
	
	return app->exec();
}
