set QTLIB=C:\Qt\Qt5.1\5.2.0\mingw48_32\lib\
set QTBIN=C:\Qt\Qt5.1\5.2.0\mingw48_32\bin\
set MINGWBIN=C:\Qt\Qt5.1\5.2.0\mingw48_32\bin\
set oldp=%cd%
set type=debug
md ..\flatboat-build-win
cd ..\flatboat-build-win
qmake ..\flatboat\flatboat.pro
mingw32-make %type%
md %type%\flatboat
copy %type%\flatboat.exe %type%\flatboat
copy %QTBIN%Qt5Core.dll %type%\flatboat & copy %QTBIN%Qt5Gui.dll %type%\flatboat & copy %QTBIN%Qt5Qml.dll %type%\flatboat & copy %QTBIN%Qt5Network.dll %type%\flatboat & copy %QTBIN%Qt5V8.dll %type%\flatboat & copy %QTBIN%Qt5Quick.dll %type%\flatboat & copy %QTBIN%Qt5Widgets.dll %type%\flatboat & copy %MINGWBIN%icudt51.dll %type%\flatboat & copy %MINGWBIN%icuuc51.dll %type%\flatboat & copy "%MINGWBIN%libstdc++-6.dll" %type%\flatboat & copy "%MINGWBIN%icuin51.dll" %type%\flatboat & copy "%MINGWBIN%libgcc_s_dw2-1.dll" %type%\flatboat & copy "%MINGWBIN%libGLESv2.dll" %type%\flatboat & copy "%MINGWBIN%libwinpthread-1.dll" %type%\flatboat & copy "%MINGWBIN%D3DCompiler_43.dll" %type%\flatboat
cd %oldp%