/* This file is part of "modern computer flatboat", a pdf viewer.
 * Copyright (C) 2012 Frank Fuhlbrück 
 * License: GPLv3 (or any later version, at your option)
 * See the file "LICENSE".
 */
import QtQuick 2.0

Rectangle {
	id: splash
	z: 2
	color: "#0E188D"
	width: splashText.implicitWidth + 30
	height: splashText.implicitHeight + logo.height + 30
	radius: 10
	anchors.centerIn: parent
	Text {
		id: splashText
		color: "white"
		font.pointSize : 22
		horizontalAlignment: Text.AlignHCenter
		anchors.bottom: parent.bottom
		anchors.bottomMargin: 15
		anchors.horizontalCenter: parent.horizontalCenter
		text: "<a align=center><b>modern computer flatboat</b><br/> a viewer for the<br/><b>portable document format</b></a>"
	}
	Image{
		id: logo
		anchors.bottom: splashText.top;
		anchors.horizontalCenter: parent.horizontalCenter
		width: splashText.width/2;
		fillMode: Image.PreserveAspectFit
		smooth: true
		source: "flatboat.png"
	}
	Timer {
		interval: sethist.value("splashSeen") ? 1500 : 8000;
		running: true;
		repeat: false
		onTriggered: {splash.visible = false;sethist.setValue("splashSeen",true)}
	}
}
