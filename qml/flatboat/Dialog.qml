/* This file is part of "modern computer flatboat", a pdf viewer.
 * Copyright (C) 2012 Frank Fuhlbrück 
 * License: GPLv3 (or any later version, at your option)
 * See the file "LICENSE".
 */
import QtQuick 2.0

Rectangle {
	id: dialog
	z: 3
	width: Math.max(yes.width + no.width +15, quest.width + 20)
	height: yes.height + quest.height + 25
	radius: 10
	clip: true
	anchors.centerIn: parent
	border.width: 4
	visible: false
	focus: visible
	
	property string yesAction;
	property string noAction;
	property string postAction;
	
	property alias question: quest.text
	property alias noText: no.text
	property alias yesText: yes.text
	
	
	function close(){
		parent.state = ""
		dialog.parent.focus = true;
	}
	signal yes();
	onYes:{eval(yesAction);close();eval(postAction);}
	signal no();
	onNo: {eval(noAction);close();eval(postAction);}
	

	Text{
		z:3
		id: quest
		anchors.top: parent.top
		anchors.left: parent.left;
		anchors.margins : 10;
		text: "OK?"
		font.pointSize: 22
	}
	
	Button{
		id:yes
		z:3
		text: "yes"
		anchors.top: quest.bottom
		anchors.left: parent.left;
		anchors.margins : 5;
		onClicked: dialog.yes();
	}
	Button {
		id:no
		anchors.top : quest.bottom
		anchors.right: parent.right;
		anchors.margins : 5;
		text: "no"
		onClicked: dialog.no();
	}

}
