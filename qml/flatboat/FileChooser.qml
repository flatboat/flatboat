/* This file is part of "modern computer flatboat", a pdf viewer.
 * Copyright (C) 2012 Frank Fuhlbrück 
 * License: GPLv3 (or any later version, at your option)
 * See the file "LICENSE".
 */
import QtQuick 2.0

Rectangle {
	id: fileChooser
	z: 2
	width: parent.width*0.95
	height: parent.height*0.95
	radius: 10
	clip: true
	anchors.centerIn: parent
	border.width: 4
	visible: false
	focus: visible
	onVisibleChanged: {
		if(visible)
			fill();
		else
			parent.focus = true;
	}
	function close(){
		parent.state = "";
	}
	function fill(){
		modl.clear();
		for(var i=0;i<sethist.documents.length;i++)
			modl.append(sethist.documents[i]);
	}
	signal openFile();
	
	Button{
		id:fileopen
		z:3
		text: "open new file"+(sethist.platform() == "maemo5" ? 
			" (dir. of last file)" :"")
			
		anchors.top: parent.top;
		anchors.left: parent.left;
		anchors.right: close.left;
		anchors.margins : 5;
		onClicked: {
			if(!sethist.chooseFile(""))
				return;
			fileChooser.openFile();
			fileChooser.close()
		}
	}
	Button{
		id:fileopenMD
		z:3
		text: "open new file (root dir.)"
		visible: sethist.platform() == "maemo5"
		anchors.top: fileopen.bottom;
		anchors.left: parent.left;
		anchors.right: close.left;
		anchors.margins : 5;
		onClicked: {
			if(!sethist.chooseFile("/"))
				return;
			fileChooser.openFile();
			fileChooser.close()
		}
	}
	Button {
		id: close 
		anchors.top : parent.top
		anchors.right: parent.right;
		anchors.margins : 5;
		text: "x"
		onClicked: fileChooser.close();
	}
	Text{
		z:3
		id: lastFilesText
		anchors.top : sethist.platform() == "maemo5" ? fileopenMD.bottom : fileopen.bottom 
		anchors.left: parent.left;
		anchors.right: parent.right;
		anchors.margins : 5;
		text: "last viewed documents:"
		font.pointSize: 22
	}
	ListView{
		id: doclist
		height: 0.95*fileChooser.height; 
		flickableDirection: Flickable.VerticalFlick
		boundsBehavior: Flickable.StopAtBounds
		snapMode: ListView.SnapToItem
		

		anchors.top : lastFilesText.bottom;
		anchors.left: parent.left;
		anchors.right: parent.right;
		anchors.margins : 5;
		anchors.bottom:clearList.top
		clip: true
		model: ListModel{
			id:modl	
		}
		delegate: Button{
			text: ( (index>0&&index<=9?index:"") + " " +  ((typeof title !== "undefined") ?title:"") +
				((typeof title !== "undefined")?" (":"") +
				filepath.substr(filepath.lastIndexOf("/")+1) +
				((typeof title !== "undefined")?")":"") +
				"<br/><a style='font-size:10pt'> in " +
				filepath.substr(0,filepath.lastIndexOf("/")) +
				"</a>")
			wrapMode: Text.Wrap
			centerText: false
			width: parent ? parent.width : 0
// 			anchors.left : parent.left
// 			anchors.right: parent.right
			pointSize: mainRec.refPtSize * 2/3;
			onClicked: {
				sethist.newDocument({filepath:filepath});
				fileChooser.openFile();
				fileChooser.close();
			}
			rightText: index != 0 ? "-":"";
			onRightTextClicked: {
				sethist.removeDocument(index);
				fileChooser.fill();
			}
		}
	}
	
	Button{
		id:clearList
		z:3
		text: "clear list"
		anchors.bottom: parent.bottom;
		anchors.left: parent.left;
		anchors.right: parent.right;
		anchors.margins : 5;
		onClicked: {sethist.clearDocuments();fileChooser.fill()}
	}

}
