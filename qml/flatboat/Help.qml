/* This file is part of "modern computer flatboat", a pdf viewer.
 * Copyright (C) 2012 Frank Fuhlbrück 
 * License: GPLv3 (or any later version, at your option)
 * See the file "LICENSE".
 */
import QtQuick 2.0

Rectangle {
	id: helpScreen
	z: 2
	width: parent.width*0.95
	height: parent.height*0.95
	radius: 10
	clip: true
	anchors.centerIn: parent
	border.width: 4
	visible: false
	focus: visible
	onVisibleChanged: if(!visible) parent.focus = true
	function close(){
		parent.state = "";
	}
	
	Keys.onPressed: {
		switch(event.key){
			case Qt.Key_Left:
			case Qt.Key_Up:
			case Qt.Key_F8:
				if(helpflick.contentY > 0)
					helpflick.contentY = Math.max(0,helpflick.contentY-0.8 * helpflick.height);
				event.accepted = true;
				break;
			case Qt.Key_Right:
			case Qt.Key_Down:
			case Qt.Key_F7:
				if(helpflick.contentY + helpflick.height < helpflick.contentHeight)
					helpflick.contentY += 0.8 * helpflick.height;
				event.accepted = true;
				break;
		}
	}
	

	
	Flickable{
		id: helpflick
		anchors.centerIn: parent
		width: 0.95*helpScreen.width
		height: 0.95*helpScreen.height 
		contentWidth: helpText.width
		contentHeight: helpText.height
		flickableDirection: Flickable.VerticalFlick
		boundsBehavior: Flickable.StopAtBounds
	Text {
		id: helpText
		//anchors.centerIn: parent
		width: 0.95*helpScreen.width
		wrapMode: Text.Wrap
		textFormat:Text.RichText
		onLinkActivated: {Qt.openUrlExternally(link)}
		text: "\
		<hr>\
		<big><center><b>modern computer flatboat</b><br/>a viewer for the<br/><b>portable document format</b></a></big></center>\
		<hr>\
		<p>\
		© 2012 Frank Fuhlbrück, License: GPLv3 (or later version, at your option)<br>\
		See <a href=\"http://www.gnu.org/licenses/gpl-3.0.txt\">http://www.gnu.org/licenses/gpl-3.0.txt</a><br>\
		flatboat on gitorious: <a href=\"https://gitorious.org/flatboat\">https://gitorious.org/flatboat</a><br>\
		This program uses libfitz, the library of <a href=\"http://www.mupdf.com/\">mupdf</a>.\
		</p><hr>\
		<p>\
		<h4>Modes</h4>\
		<h5>normal mode</h5>\
		The normal mode (column mode and text mode switches are off) is page oriented (e.g. next means next page), \
		therefore it's convenient to view images and get the outline of the page.\
		<h5>column mode</h5>\
		Column mode identifies horizontally overlapping text areas as columns and is designed for reading them left to right.\
		The next button, lower tap zone and the keys mapped to them scroll down until the end of the column and then switch to \
		the beginning of the next column. By changing the zoom accordingly, the column mode is best for reading text.\
		<b>Note:</b> Works well even with a single column.\
		<h5>text mode</h5>\
		Text mode is included as an emergency mode only (tiny font, column detection fails etc.), the reading experience is much worse\
		than in column mode in the general case.<b>Note:</b> Search and highlighting don't work in text mode.\
		<h4>In-document history</h4>\
		Do you know this? You frequently have to switch between two or more pages within a large document,\
		say your current chapter and the index?\
		You follow a link inside a PDF document and forgot were you came form?\
		Don't worry, because <b>modern computer flatboat</b> now comes with an in-document history.<br/>\
		You can't believe it? Well, it's actually quite simple: everytime you a) follow a link b) follow an entry in the TOC (s.b.)\
		c) go to the first or last page or d) use the jump back function, m.c. flatboat saves the page you came from. By pressing BACKSPACE\
		(or the Back Button on android) you can return to the last page from which you did one of a-d (i.e. \"jump back\"). The complete in-document history can be retrieved by pressing I or selecting the clock icon in the main menu. \
		<h4>Annotations</h4>\
		Annotation support is still somewhat preliminary: Text annotations (\"notes\") and Ink annotations (\"freehand drawing\") are supported.\
		However no appearance dictionary is written to the pdf, so some readers don't show the annotations.\
		At least Okular, Adobe Reader and OS X's Preview display them correctly.\
		<h4>Buttons, keys and input fields</h4>\
		<table border=1>\
		<tr><th>Button</th><th>Key</th><th>Function</th></tr>\
		<tr><td></td><td>M,<br/>long/double click</td><td>show/hide menu bars</td></tr>\
		<tr><td>file</td><td>O (letter),<br/>Menu (android)</td><td>open file menu</td></tr>\
		<tr><td></td><td>1-9</td><td>open nth last file</td></tr>\
		<tr><td>sd card</td><td>W</td><td>write changes <br/>(i.e. annotations) to current file</td></tr>\
		<tr><td>&lt;?</td><td></td><td>search previous appearance</td></tr>\
		<tr><td>aA</td><td></td><td>case sensitivity on/off</td></tr>\
		<tr><td>search field</td><td></td><td>highlight phrase on page (search starts with &lt;? / ?&gt; or RETURN)</td></tr>\
		<tr><td>| - + X</td><td>S</td><td>scroll direction(s)</td></tr>\
		<tr><td>show links</td><td>L</td><td>draw rectangles around links</td></tr>\
		<tr><td>[]</td><td>F</td><td>fullscreen on/off</td></tr>\
		<tr><td>TOC</td><td>U</td><td>table of contents (or outline)</td></tr>\
		<tr><td>clock symbol</td><td>I</td><td>in-document history (s.a.)</td></tr>\
		<tr><td></td><td>Backspace,<br/>Back Button (android)</td><td>goto page before last \"jump\"</td></tr>\
		<tr><td>pencil</td><td>A</td><td>annotate(only ink and text annotations for now)</td></tr>\
		<tr><td>help</td><td>H</td><td>You found out about it by now, I expect.</td></tr>\
		<tr><td colspan=3></td></tr>\
		<tr><td>page number</td><td></td><td>change it to switch the page</td></tr>\
		<tr><td>|&lt;</td><td>B, left long <br/> press (android)</td><td>go to first page</td></tr>\
		<tr><td>&lt;</td><td>Left,Up,VolUp</td><td>previous page or (part of) column </td></tr>\
		<tr><td>&gt;|</td><td>N, right long <br/> press (android)</td><td>go to last page</td></tr>\
		<tr><td>+/-</td><td>+/-</td><td>zoom in/out by 3% (of current zoom)</td></tr>\
		<tr><td>zoom</td><td></td><td>4 zoom options:<br/>100%,fit page, fit height,<br/> strecth (ignore aspect ratio)</td></tr>\
		<tr><td></td><td>0 (number)</td><td>set zoom to 1=100%</td></tr>\
		<tr><td>zoom in percent</td><td></td><td>set zoom explicitly</td></tr>\
		<tr><td>mode switches</td><td>C,T</td><td>column / text mode (s.a.)</td></tr>\
		<tr><td>quit</td><td>CTRL+Q</td><td>exit the application</td></tr>\
		<tr><td colspan=3><b>while in annotation mode:</b></td></tr>\
		<tr><td>colored rectangle</td><td></td><td>color picker</td></tr>\
		<tr><td>sheet of paper</td><td>N</td><td>add a note (text annotation)</td></tr>\
		<tr><td>pencil</td><td>D</td><td>add a freehand line (ink annotation)</td></tr>\
		<tr><td>eraser</td><td>E</td><td>remove an annotation</td></tr>\
		</table>\
		</p>\
		"
	}
	}

	Button {
		id: close 
		anchors.top : parent.top
		anchors.right: parent.right;
		anchors.margins : 5;
		text: "x"
		onClicked: helpScreen.close();
	}
}
