/* This file is part of "modern computer flatboat", a pdf viewer.
 * Copyright (C) 2012 Frank Fuhlbrück 
 * License: GPLv3 (or any later version, at your option)
 * See the file "LICENSE".
 */
import QtQuick 2.0

Rectangle {
	id: inDocHist
	z: 2
	width: parent.width*0.95
	height: parent.height*0.95
	property variant outline;
	radius: 10
	clip: true
	anchors.centerIn: parent
	border.width: 4
	visible: false
	focus: visible
	onVisibleChanged: {
		if(visible)
			fill();
		else
			parent.focus = true;
	}
	function close(){
		parent.state = "";
	}
	function fill(){
		modl.clear();
		if(!sethist.curDoc.pageHistory)
			return;
		for(var i=0;i<sethist.curDoc.pageHistory.length;i++)
			modl.append({pn : sethist.curDoc.pageHistory[i]});
	}
	signal requestGoBack(int entry);
	signal requestClear();
	signal requestRemoveEntry(int entry);
	function findPageInOutline(outl,pn){
		if(outl.page>pn)
			return "";
		if(!outl.sub)
			return outl.title?outl.title:"";
		var i = 0;
		for(;i<outl.sub.length && outl.sub[i].page <= pn;i++) {}
		if(i>0)
			return findPageInOutline(outl.sub[i-1],pn);
		else
			return outl.title?outl.title:"";
	}
	

	
	Text{
		id: headline
		anchors.top : parent.top
		anchors.left: parent.left;
		anchors.margins : 5;
		width: parent.width
		clip: false
		wrapMode:Text.Wrap
		text: "in-document history"
		font.pointSize: 22
	}
	Text{
		id: docName
		anchors.top : headline.bottom
		anchors.left: parent.left;
		anchors.margins : 5;
		width: parent.width
		clip: false
		textFormat: Text.RichText
		wrapMode:Text.Wrap
		text: (sethist.curDoc.title?sethist.curDoc.title:sethist.curDoc.filepath) +
		(sethist.curDoc.author?("<br/>by " +sethist.curDoc.author):"")
		font.pointSize: 14
	}
	Button {
		id: close 
		anchors.top : parent.top
		anchors.right: parent.right;
		anchors.margins : 5;
		text: "x"
		onClicked: inDocHist.close();
	}
	ListView{
		id: pagelist
		height: 0.95*inDocHist.height; 
		flickableDirection: Flickable.VerticalFlick
		boundsBehavior: Flickable.StopAtBounds
		snapMode: ListView.SnapToItem
		

		anchors.top : docName.bottom;
		anchors.left: parent.left;
		anchors.right: parent.right;
		anchors.margins : 5;
		anchors.bottom:clearList.top
		clip: true
		model: ListModel{
			id:modl	
		}
		
		delegate: Button{
			property string outlTitle : findPageInOutline({page:1,sub:outline},pn)
			text: /*(index>0?index:"") +*/ " page " + pn + (outlTitle?" (in: "+outlTitle+")":"");
			wrapMode: Text.Wrap
			centerText: false
			width: parent ? parent.width : 0
// 			anchors.left : parent.left
// 			anchors.right: parent.right
			pointSize: mainRec.refPtSize * 2/3;
			onClicked: {
				inDocHist.requestGoBack(index);
				inDocHist.close();
			}
			rightText: "-"
			onRightTextClicked: {
				requestRemoveEntry(index);
				inDocHist.fill()
			}
		}
		

	}

	
	Button{
		id:clearList
		z:3
		text: "clear list"
		anchors.bottom: parent.bottom;
		anchors.left: parent.left;
		anchors.right: parent.right;
		anchors.margins : 5;
		onClicked: {requestClear();inDocHist.fill()}
	}

}
