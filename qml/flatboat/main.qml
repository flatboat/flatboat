/* This file is part of "modern computer flatboat", a pdf viewer.
 * Copyright (C) 2012,2013 Frank Fuhlbrück 
 * License: GPLv3 (or any later version, at your option)
 * See the file "LICENSE".
 */
import QtQuick 2.0
import flatboat.PdfImage 1.0

Rectangle {
	id: mainRec
	width: 480
	height: 800
	signal showFullScreen()
	signal showNormal()
	signal fileOpen(string filepath)
	signal windowTitleChangeRequest(string title)
// 	signal updateRequest()
	property string windowTitle : "";
	onWindowTitleChanged: windowTitleChangeRequest(windowTitle);
	property int refWidth: tocButton.width;
	property int refHeight: searchPrev.height; 
	property int refPtSize: searchPrev.pointSize;
	onWidthChanged: if(flick.columnMode) flick.columnModeChanged();
	onHeightChanged: if(flick.columnMode) flick.columnModeChanged();
	
	onFileOpen: {
		sethist.log(filepath);
		sethist.newDocument({filepath:filepath});
		if(pdf.modified) {
			askWrite("loadFileAndRestoreSettings()");
		} else loadFileAndRestoreSettings();
	}
	
	FileChooser {
		id:fileChooser
		onOpenFile: {
			//don't check pdf.modified here
			//was done earlier
			loadFileAndRestoreSettings();
		}
	}
	
	InDocumentHistory {
		id:inDocHist
		outline: pdf.outline
		onRequestGoBack: {
			goBackInHistory(entry);
		}
		onRequestClear:{
			curDocSet("pageHistory",new Array())
		}
		onRequestRemoveEntry: {
			curDocSet("pageHistory",sethist.curDoc.pageHistory.slice(0,entry).concat(sethist.curDoc.pageHistory.slice(entry+1)))
		}
	}
	
	TableOfContents {
		id:tocView
		outline: pdf.outline
		onRequestPageJump: {
			jumpToPage(pn);
		}
	}
	
	AnnotationBar {
		id: annoBar
		refLength: mainRec.refHeight;
		property int oldFlickMode: 0;
		onDrawModeChanged:{
			pdf.drawMode=drawMode;
			if(drawMode){
				oldFlickMode = flickSel.fstate;
				flickSel.fstate = flickSel.none - 1//the one before none
				flickSel.clicked();//none: no flicking
			} else {
				flickSel.fstate = oldFlickMode - 1//the one before oldFlickMode
				flickSel.clicked();//now: oldFlickMode
			}
		}
		onEraseModeChanged: {
			pdf.eraseMode=eraseMode;
		}
		onNoteModeChanged: {
			pdf.noteMode=noteMode;
			if(noteMode)
				mainRec.Keys.enabled = false;
			else
				mainRec.focus = mainRec.Keys.enabled = true;
		}
		
		onNoteTextChanged: pdf.updateAnnoText(noteText);
		
	}
	
	Dialog {
		id: awDialog
		visible: false
		question: "Do you want to save your changes?"
		noText: "do not save"
		yesText: "save"
		yesAction: "pdf.writeCurrentFile()";
	}
	
	function askWrite(postAction){
		if(pdf.modified){
			awDialog.postAction = postAction;
			mainRec.state = "modalDialog"
		} else
			eval(postAction);
	}
	
	
	function loadFileAndRestoreSettings(){
		if(!pdf.loadFile(sethist.curDoc.filepath))
			return;	
		//don't render each time, since we change multiple attributes 
		pdf.gotoPage(sethist.curDoc.lastPage ? sethist.curDoc.lastPage : 1,false);
		pdf.setZoom(sethist.curDoc.lastZoom ? sethist.curDoc.lastZoom : 100,false);
		pdf.askForRendering();
		if(sethist.curDoc.lastMode == "textMode")
			txtToggle.state = "active";
		else if(sethist.curDoc.lastMode == "colMode"){
			colToggle.state = "active";
			pdf.regionIdx = sethist.curDoc.lastRegionIdx ? sethist.curDoc.lastRegionIdx : 0;
			var r = pdf.curRegion();
			flick.contentX=r.x-10;
			flick.contentY = sethist.curDoc.lastContentY ? sethist.curDoc.lastContentY : r.y;
		} else {
			colToggle.state = "";
			txtToggle.state = "";
		}
		curDocSet("title",pdf.title);
		windowTitle = "m.c.flatboat : " + (pdf.title?pdf.title:sethist.curDoc.filepath);
		curDocSet("author",pdf.author);
	}
	
	function curDocSet(key,val){
		var curd = sethist.curDoc;
		curd[key] = val;
		sethist.curDoc = curd;
// 		console.log(key+":");
// 		console.log(sethist.curDoc[key]);
	}
	
	function openPreviousDoc(idx){
		if(sethist.documents.length <= idx)
			return;
		//TODO:inefficient
		sethist.newDocument(sethist.documents[idx]);
		if(pdf.modified){
			askWrite("loadFileAndRestoreSettings()");//true: "open file" pending
		} else loadFileAndRestoreSettings();
	}
	
	//gotoPage + save the last page in History
	function jumpToPage(newPage){
		if(pdf.pagenum == newPage)
			return;
		var pageHist = sethist.curDoc.pageHistory ? Array().concat(sethist.curDoc.pageHistory) : new Array();
		
		pageHist.unshift(pdf.pagenum);//prepend current pagenum
		curDocSet("pageHistory",pageHist);
		pdf.pagenum = newPage;
		//TODO:dirty (doesn't really belong here)
		flick.columnModeChanged();
	}
	
	//NOTE: this history works slightly different than a browser history
	// e.g.: if you go back 2 times, you're where you started
	//this allows quick jumping between e.g. the literature appendix and
	//your current section
	function goBackInHistory(entry){
		var pageHist = Array().concat(sethist.curDoc.pageHistory);
		if(!pageHist || pageHist.length <= entry)
			return;
		var newPage = pageHist[entry]
		if(newPage === null)
			return;
		pageHist.splice(entry,1);
		if(pdf.pagenum == newPage)//length of history decreases,
			return;//beacause saved page was reached "manually"
		if(pageHist[0] != pdf.pagenum)//pageHist[0] may be undefined, but pagenum >= 1
			pageHist.unshift(pdf.pagenum);
		curDocSet("pageHistory",pageHist);
		pdf.pagenum = newPage;
		//TODO:dirty (doesn't really belong here)
		flick.columnModeChanged();
	}
	
	function allInputFieldsOff(){
		pageIpt.off();
		searchWhat.off();
		zoomIpt.off();
	}
	
	focus: true
	
	Keys.onPressed:{
		//sethist.log(event.key + " pressed");
		if(sethist.platform() == "android")
			longPressTimer.waitForLongPress(event.key);
		if(event.key == Qt.Key_Close)
			event.accepted = true;
	}
	Timer {
		id: longPressTimer
		interval: 800
		repeat: false
		property int key;
		property bool fired;
		function waitForLongPress(k){
			//sethist.log(Qt.Key_Menu + " k");
			key = k
			//sethist.log(key + " key");
			start();//not restart, since android generates repeats early (but only pressed)
		}
		onTriggered: {
			//sethist.log(key + " triggered");
			fired = true;
			switch(key){
				case Qt.Key_Menu-1://strange bug (alpha4 too)
				case Qt.Key_Menu:
					choose.clicked();
					break;
				case Qt.Key_Close:
					iDHButton.clicked();
					break;
				case Qt.Key_Left:
					first.clicked();
					break;
				case Qt.Key_Right:
					last.clicked();
					break;
			}
		}
	}
	Keys.onReleased: {
		//sethist.log(event.key + " released");
		longPressTimer.stop();
		if(longPressTimer.fired){
			longPressTimer.fired = false;
			return;
		}
		switch(event.key){
			case Qt.Key_Left:
			case Qt.Key_Up:
			case Qt.Key_F8:
				if(helpScreen.visible)
					return;
				prev.clicked();
				event.accepted = true;
				break;
			case Qt.Key_Right:
			case Qt.Key_Down:
			case Qt.Key_F7:
				if(helpScreen.visible)
					return;
				next.clicked();
				event.accepted = true;
				break;
			case Qt.Key_B://B,N are neighbours in many western keyboards
				first.clicked();
				break;
			case Qt.Key_N:
				if(annoBar.visible)
					annoBar.switchMode("note");
				else
					last.clicked();
				break;
			case Qt.Key_C:
				colToggle.toggleState();
				break;
			case Qt.Key_T:
				txtToggle.toggleState();
				break;
			case Qt.Key_S:
				flickSel.clicked();
				break;
			case Qt.Key_L:
				linkHintToggle.toggleState();
				break;
			case Qt.Key_Equal:
			case Qt.Key_Plus:
				zoom_p.clicked();
				break;
			case Qt.Key_Minus:
				zoom_m.clicked();
				break;
			case Qt.Key_0://the number
				zoom_0.clicked();
				break;
			case Qt.Key_W:
				writeButton.checkModified();
				writeButton.clicked();
				break;
			case Qt.Key_M:
			case Qt.Key_Menu:
				flickma.clicked({});
				break;
			case Qt.Key_A:
				if(!annoBar.visible)
					annoButton.clicked({});
				else
					annoBar.close();
				break;
			case Qt.Key_D:
				if(annoBar.visible)
					annoBar.switchMode("draw");
				break;
			case Qt.Key_E:
				if(annoBar.visible)
					annoBar.switchMode("erase");
				break;
			case Qt.Key_H:
				if(helpScreen.visible) 
					helpScreen.close();
				else
					showHelp.clicked();
				break;
			case Qt.Key_O://the letter
				if(fileChooser.visible) 
					fileChooser.close();
				else
					choose.clicked();
				break;
			case Qt.Key_U:
				if(tocView.visible) 
					tocView.close();
				else
					tocButton.clicked();
				break;
			case Qt.Key_I:
				if(inDocHist.visible) 
					inDocHist.close();
				else 
					iDHButton.clicked();
				break;
			case Qt.Key_F:
				fullsc.clicked();
				break;
			case Qt.Key_Q:
				if(event.modifiers & Qt.ControlModifier)
					Qt.quit();
				break;
			case Qt.Key_1:
			case Qt.Key_2:
			case Qt.Key_3:
			case Qt.Key_4:
			case Qt.Key_5:
			case Qt.Key_6:
			case Qt.Key_7:
			case Qt.Key_8:
			case Qt.Key_9:
				openPreviousDoc(event.key - Qt.Key_0);
				if(fileChooser.visible) 
					fileChooser.close();
				break;
			case Qt.Key_Backspace:
				goBackInHistory(0);
				break;
			case Qt.Key_Escape:
			case Qt.Key_Close://back button on android, requires necessitas alpha4
				if(helpScreen.visible) 
					helpScreen.close();
				else if(fileChooser.visible) 
					fileChooser.close();
				else if(inDocHist.visible)
					inDocHist.close();
				else if(tocView.visible){
					tocView.close();
				}else if (mainRec.state == "menus")
					flickma.clicked({});
				else if(event.key != Qt.Key_Escape)//not intended on Desktop
					goBackInHistory(0);
				
				//important on Android, otherwise app is closed
				event.accepted = true;
				sethist.log("back");
				break;
		}
 		sethist.log(event.key)
	}

	Splash {}
	Help {id:helpScreen}

	Text {
		id: flickPosText
		visible: false
		anchors.centerIn: parent
		font.pointSize: 16;
		text: "("+ Math.floor(100*flick.contentX/flick.movableWidth)+","+
		Math.floor(100*flick.contentY/flick.movableHeight) + ")";
	}
	Flickable {
		id: flick
		anchors.fill: parent;
		contentWidth: pdf.width
		contentHeight: pdf.height
		property int movableWidth: Math.max(contentWidth-width,0);
		property int movableHeight: Math.max(contentHeight-height,0);
		flickableDirection: Flickable.VerticalFlick
		boundsBehavior: Flickable.StopAtBounds
		property bool columnMode : colToggle.bstate;
		onColumnModeChanged: {
			if(columnMode) {
				curDocSet("lastMode","colMode");
				pdf.zoom *= 0.95*width/pdf.curRegion().width;//redraws page=>curRegion changes  
				var r = pdf.curRegion();
				contentX=r.x-10;
				contentY=r.y; 
			} else if (!pdf.textMode)
				curDocSet("lastMode","");
		}
		
		Component.onCompleted: {
			if(!sethist.curDoc.filepath)
				return;
			loadFileAndRestoreSettings();
		}
		
		onMovementEnded: {
			flickPosText.visible = false;
			pdf.grainyMode = false;
			if(columnMode)
				curDocSet("lastContentY",contentY);
		}
		
		onMovementStarted : {
			pdf.stopClickTimer();
			if (sethist.displayType() == "eink"){
				flickPosText.visible = true;
				pdf.grainyMode = true;
			}
			//hopefully sometime:
			//sethist.setUpdateMode(5);
		}
		
		MouseArea {
			id: flickma
			anchors.fill: parent
			onDoubleClicked: clicked()
			//hoverEnabled : true
			onClicked: {
				if(mainRec.state != "menus"){
					mainRec.state = "menus";
// 					annoBar.close();
// 					fileChooser.close();
				}
				else
					mainRec.state = "";
			}
		}
		
		PdfImage {
			id: pdf
			highlightPhrase: searchWhat.text
			caseSensitive: caseSensitiveToggle.bstate //state as bool
			textMode: txtToggle.bstate
			linkHints: linkHintToggle.bstate
			drawColor: annoBar.choosenColor
			onHitRectChanged: {
				if(hitRect.x < flick.contentX ||
					hitRect.x > flick.contentX + flick.width - hitRect.width)
					flick.contentX = Math.max(hitRect.x - flick.width/2,0);
				if(hitRect.y < flick.contentY ||
					hitRect.y > flick.contentY + flick.height - hitRect.height)
					flick.contentY = Math.max(hitRect.y - flick.height/2,0);
			}
			onTextModeChanged: {
				if(textMode) {
					curDocSet("lastMode","textMode")
					flick.contentX = 0;
					flick.contentY = 0;
				}
				else if(!flick.columnMode)
					curDocSet("lastMode","");
			}
			onPagenumChanged: {
				curDocSet("lastPage",pn);
				if(!flick.columnMode)/*flick.contentX=*/flick.contentY=0;
			}
			onZoomChanged: {
				curDocSet("lastZoom",zm);
				if(flick.columnMode){
					var r = pdf.curRegion(); 
					flick.contentX=r.x-10;
					//flick.contentY=r.y
				}
			}
			onClickForward: if(!flick.moving) flickma.clicked({});
			onAllPressForward: mainRec.allInputFieldsOff();
			onLinkSelected: {
				if(typeof destination == "number")
					jumpToPage(destination > pdf.totalPages ? pdf.totalPages : destination);
				else if(typeof destination == "string")
					Qt.openUrlExternally(destination);
			}
			onNewNote: annoBar.clearNoteTextInput();
		}
		
		function prevRegion_orUp(){
			var r = pdf.curRegion();
			if(contentY > r.y){
				contentY -= 0.8 * flick.height;
				curDocSet("lastContentY",contentY);
			} else {
				r = pdf.prevRegion(false);//dont't render, since followed by a zoom
				pdf.setZoom(pdf.zoom*0.95*width/r.width,false)//chnages page=>curRegion changes
				pdf.askForRendering();
				r = pdf.curRegion();
				curDocSet("lastRegionIdx",pdf.regionIdx);
				contentX=r.x-10;
				contentY=r.y+r.height-0.9*height;
				curDocSet("lastContentY",contentY);
			}
		}
		function nextRegion_orDown(){
			var r = pdf.curRegion();
			if(contentY + height < r.y + r.height){
				contentY += 0.8 * height;
				curDocSet("lastContentY",contentY);
			} else {
				r = pdf.nextRegion(false);//dont't render, since followed by a zoom
				pdf.setZoom(pdf.zoom*0.95*width/r.width,false)//chnages page=>curRegion changes
				pdf.askForRendering();
				r = pdf.curRegion();
				curDocSet("lastRegionIdx",pdf.regionIdx);
				contentX=r.x-10;
				contentY=r.y;
				curDocSet("lastContentY",contentY);
			}
		}
	}
	
	Rectangle{
		id: uprTapZone
		width: parent.width
		color: Qt.rgba(0, 0, 0, 0.1)
		anchors.top: parent.top
		height: {
			"maemo5": flick.height/18,
			"android":flick.height/30,
			"linux":flick.height/100,
			"macx":flick.height/100,
			"win32":flick.height/100,
			"default":flick.height/35}[sethist.platform()]
		MouseArea {
			id: ma_prev
			anchors.fill: parent;
			onClicked: {
				prev.clicked();
			}
		}
	}
	
	Rectangle{
		width: parent.width
		color: Qt.rgba(0, 0, 0, 0.1)
		anchors.bottom: parent.bottom
		height: uprTapZone.height
		MouseArea {
			id: ma_next
			anchors.fill: parent;
			onClicked: {
				next.clicked();
			}
		}
	}

	
	Flow {
		id: lwrMenu
		z : 1
		visible: false
		spacing: 5
		anchors.left: parent.left
		anchors.leftMargin: 20
		anchors.right: parent.right
		anchors.rightMargin: 20
		anchors.bottom: parent.bottom
		anchors.bottomMargin: 30
		
		InputField {
			id: pageIpt
			height: mainRec.refHeight
			//width:90
			inputMask: "000000"
			text: pdf.pagenum
			postText: "/"+pdf.totalPages
			onAccepted: jumpToPage(pageIpt.text > pdf.totalPages ? pdf.totalPages : pageIpt.text);
			onFocusLost: {mainRec.focus = mainRec.Keys.enabled = true}
			onFocusGained: {mainRec.Keys.enabled = false}
		}
		
		Button {
			id : first
			//text: "|<"
			image: "first"
			width: mainRec.refHeight
			onClicked: jumpToPage(1);
		}
		Button {
			id : prev
			image: "prev"
			width: mainRec.refHeight
			//text: "<"
			onClicked: { 
				if(flick.columnMode)
					flick.prevRegion_orUp();
				else 
					pdf.pagenum = pdf.pagenum > 1 ? pdf.pagenum - 1 : pdf.pagenum;
			}
		}
		Button {
			id : next
			image: "next"
			//text: ">"
			width: mainRec.refHeight
			onClicked: { 
				if(flick.columnMode)
						flick.nextRegion_orDown();
				else 
					pdf.pagenum++;
			}
		}
		Button {
			id : last
			image: "last"
			width: mainRec.refHeight
			//text: ">|"
			onClicked: jumpToPage(pdf.totalPages)
		}
		Button {
			id : zoom_p
			state: flick.columnMode ? "disabled" : ""
			//text: "z+"
			image: "zoomplus"
			width: mainRec.refHeight
			height: mainRec.refHeight
			onClicked: {
				if(pdf.textMode)
					pdf.textModeFontSize++;
				else
					pdf.zoom *= 1.08
			}
		}
		Button {
			id : zoom_sel
			state: pdf.textMode || flick.columnMode ? "disabled" : ""
			//text: "z o\no m"
			image: "zoom"
			width: mainRec.refHeight
			height: mainRec.refHeight
			pointSize: searchPrev.pointSize * 0.7;
			lineHeight: 0.8
			onClicked: zoom_menu.visible = !zoom_menu.visible
		}
		Button {
			id : zoom_m
			state: flick.columnMode ? "disabled" : ""
			//text: "z-"
			image: "zoomminus"
			width: mainRec.refHeight
			height: mainRec.refHeight
			onClicked: {
				if(pdf.textMode)
					pdf.textModeFontSize--;
				else
					pdf.zoom /= 1.08
			}
		}
		InputField {
			id: zoomIpt
			height: mainRec.refHeight
			inputMask: "000"
			width:60
			text: pdf.zoom
			onAccepted: {if(text > 50) pdf.zoom = text}
			onFocusLost: {mainRec.focus = mainRec.Keys.enabled = true}
			onFocusGained: {mainRec.Keys.enabled = false}
		}
		Button {
			id: colToggle
			//text: "  col\nmode"
			image: "colmode"
			width: mainRec.refHeight
			height: mainRec.refHeight
			pointSize: mainRec.refPtSize / 2;
			onClicked: toggleState();
			mutex: txtToggle.bstate;
		}
		Button {
			id: txtToggle
			text: " text\nmode"
			width: mainRec.refWidth
			height: mainRec.refHeight
			pointSize: mainRec.refPtSize * 0.65;
			onClicked: toggleState();
			mutex: colToggle.bstate;
		}
		Button {
			id : close_btn
			text: "quit"
			width: mainRec.refWidth
			onClicked: Qt.quit();
		}
	}
	
	Column{
		id: zoom_menu
		property bool activatable: lwrMenu.visible && !flick.columnMode && !pdf.textMode
		onActivatableChanged: if(!activatable) visible=false
		visible: false
		anchors.bottom: lwrMenu.top
		anchors.bottomMargin: 5
		anchors.left: lwrMenu.left
		anchors.leftMargin: pageIpt.width+5*(mainRec.refHeight+lwrMenu.spacing)
		
		property int refWidth: zoom_fh.width;
		
		Button {
			id : zoom_0
			text: "100%"
			width: zoom_menu.refWidth
			onClicked: {
				if(pdf.textMode)
					pdf.textModeFontSize = 12;
				else
					pdf.zoom = 100
			}
		}
		Button {
			id : zoom_fw
			text: "fit width"
			width: zoom_menu.refWidth
			onClicked: pdf.zoom = 100 *  mainRec.width/pdf.originalWidth
		}
		Button {
			id : zoom_fh
			text: "fit height"
			onClicked: pdf.zoom = 100 * mainRec.height/pdf.originalHeight
		}
		Button {
			id : zoom_fp
			text: "fit page"
			width: zoom_menu.refWidth
			onClicked: pdf.zoom = 100 * Math.min(mainRec.width/pdf.originalWidth,mainRec.height/pdf.originalHeight)
		}
		Button {
			id : zoom_stretch
			text: "stretch"
			width: zoom_menu.refWidth
			onClicked: pdf.stretch(mainRec.width,mainRec.height)
		}
	}
	
	Flow {
		id: uprMenu
		z : 1
		visible: false
		spacing: 5
		anchors.left: parent.left
		anchors.leftMargin: 20
		anchors.right: parent.right
		anchors.rightMargin: 20
		anchors.top: parent.top
		anchors.topMargin: 10
		
		
		Button {
			id: choose
			text: "file"
			width: mainRec.refWidth
			onClicked: 
				askWrite("mainRec.state = 'fileChooser';");
		}
		Button {
			id: writeButton
			state: "disabled"
			image: "save"
			visible: parent.visible
			onVisibleChanged: checkModified()
			function checkModified(){
				if(pdf.modified)//has no notifier
					state = "";
				else
					state="disabled";
			}
			width: mainRec.refHeight
			onClicked: {
				pdf.writeCurrentFile()
				if(pdf.modified)
					state = "";
				else
					state="disabled";
			}
		}
		Button {
			id: searchPrev
			state: pdf.textMode ? "disabled" : ""
			text: "<?"
			width: mainRec.refHeight
			onClicked: pdf.searchPrev(searchWhat.text);
		}
		Button {
			id: caseSensitiveToggle
			state: pdf.textMode ? "disabled" : ""
			text: "a  \n  A"
			height: mainRec.refHeight
			width: mainRec.refHeight
			pointSize: searchPrev.pointSize / 2;
			onClicked: toggleState();
		}
		Button {
			id: searchNext
			state: pdf.textMode ? "disabled" : ""
			text: "?>"
			width: mainRec.refHeight
			onClicked: pdf.searchNext(searchWhat.text);
		}
		InputField {
			id: searchWhat
			state: pdf.textMode ? "disabled" : ""
			width: 120
			height: searchPrev.height
			onFocusLost: {mainRec.focus = mainRec.Keys.enabled = true}
			onFocusGained: {mainRec.Keys.enabled = false}
			onAccepted: searchNext.clicked();
		}
		Button {
			id: flickSel
			//text: "|"
			image: "scrollver"
			property int none:0;
			property int ver:1;
			property int hor:2;
			property int both:3;
			
			property int fstate: ver;
			width: mainRec.refHeight
			onClicked: {
				switch(fstate = (fstate+1)%4){
					case hor : image="scrollhor";flick.flickableDirection = Flickable.HorizontalFlick;break;
					case both: image="scrollboth";flick.flickableDirection = Flickable.HorizontalAndVerticalFlick;break;
					case none: image="scrollnone";flick.interactive=false;break;
					case ver : image="scrollver";flick.interactive=true;flick.flickableDirection = Flickable.VerticalFlick;break;
				};
			}
		}
		Button {
			id: linkHintToggle
			state: pdf.textMode ? "disabled" : ""
			text: "show\nlinks"
			width: mainRec.refWidth
			height: mainRec.refHeight
			pointSize: mainRec.refPtSize * 0.65;
			onClicked: toggleState();
		}
		Button {
			id: fullsc
			//text: "[]"
			image: "fullscreen"
			width: mainRec.refHeight
			onClicked: {
				toggleState();
				if(state == "active")
					mainRec.showFullScreen();
				else 
					mainRec.showNormal();
			}
		}
		Button {
			id: tocButton
			text: "TOC"
			//width: mainRec.refWidth
			height: mainRec.refHeight
			pointSize: searchPrev.pointSize * 0.75;
			onClicked: {mainRec.state = "toc"}
		}
		Button {
			id: iDHButton
			//text: "inDoc\nhist"
			image: "history"
			width: mainRec.refHeight
			height: mainRec.refHeight
			pointSize: searchPrev.pointSize * 0.45;
			onClicked: {mainRec.state = "iDH"}
		}
		Button {
			id: annoButton
			state: pdf.textMode ? "disabled" : ""
			//text: "anno\ntate"
			image: "pencil"
			width: mainRec.refHeight
			height: mainRec.refHeight
			pointSize: searchPrev.pointSize * 0.65;
			onClicked: mainRec.state = "anno"
		}
		Button {
			id: showHelp
			text: "help"
			width: mainRec.refWidth
			height: mainRec.refHeight
			pointSize: mainRec.refPtSize * 0.7;
			onClicked: {mainRec.state = "helpScreen"}
		}
		
	}
	
	states: [
	State {
		name: "menus"
		
		PropertyChanges {target: lwrMenu; visible: true }
		PropertyChanges {target: uprMenu; visible: true }
		PropertyChanges {target: ma_prev; enabled: false }
		PropertyChanges {target: ma_next; enabled: false }
	},
	State {
		name: "anno"
		
		PropertyChanges {target: annoBar; visible: true }
		PropertyChanges {target: ma_prev; enabled: false }
		PropertyChanges {target: ma_next; enabled: false }
	},
	State {
		name: "fileChooser"
		
		PropertyChanges {target: fileChooser; visible: true }
		PropertyChanges {target: ma_prev; enabled: false }
		PropertyChanges {target: ma_next; enabled: false }
	},
	State {
		name: "iDH"
		
		PropertyChanges {target: inDocHist; visible: true }
		PropertyChanges {target: ma_prev; enabled: false }
		PropertyChanges {target: ma_next; enabled: false }
	},
	State {
		name: "toc"
		
		PropertyChanges {target: tocView; visible: true }
		PropertyChanges {target: ma_prev; enabled: false }
		PropertyChanges {target: ma_next; enabled: false }
	},
	State {
		name: "helpScreen"
		
		PropertyChanges {target: helpScreen; visible: true }
		PropertyChanges {target: ma_prev; enabled: false }
		PropertyChanges {target: ma_next; enabled: false }
	},
	State {
		name: "modalDialog"
		
		PropertyChanges {target: awDialog; visible: true }
		PropertyChanges {target: flick; opacity: 0.2 }
		PropertyChanges {target: ma_prev; enabled: false }
		PropertyChanges {target: ma_next; enabled: false }
	}
	]
}
