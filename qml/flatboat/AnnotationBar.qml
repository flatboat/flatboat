/* This file is part of "modern computer flatboat", a pdf viewer.
 * Copyright (C) 2012 Frank Fuhlbrück 
 * License: GPLv3 (or any later version, at your option)
 * See the file "LICENSE".
 */
import QtQuick 2.0

Item {
	id: annoBar
	z : 1
	visible: false
	anchors.right: parent.right
	anchors.rightMargin: 20
	anchors.top: parent.top
	anchors.topMargin: 10
	property int refLength;
	property alias drawMode: drawButton.bstate//boolean state
	property alias eraseMode: eraseButton.bstate//boolean state
	property alias noteMode: noteButton.bstate//boolean state
	property alias noteText: noteText.text
	property color choosenColor: "black";
	
	function clearNoteTextInput(){
		noteText.text = ""
	}
	
	function switchMode(which){
		switch(which){
			case "draw":
				drawButton.toggleState();
				break;
			case "erase":
				eraseButton.toggleState();
				break;
			case "note":
				noteButton.toggleState();
				break;
		}
	}
	
	
	onVisibleChanged:{
		if(!visible) {
			drawButton.state="";
			eraseButton.state="";
			noteButton.state="";
		}
	}
	
	function close(){
		parent.state = "";
	}
	
	Button {
		id: pickerButton
		color: choosenColor
		anchors.right: noteButton.left
		anchors.rightMargin: 5
		width: annoBar.refLength
		height: annoBar.refLength
		onClicked: colorPicker.visible = !colorPicker.visible
	}
	
// 	Button {
// 		id: noteButton
// 		text: "T"
// 		anchors.right: drawButton.left
// 		anchors.rightMargin: 5
// 		width: annoBar.refLength
// 		height: annoBar.refLength
// 		onClicked: toggleState();
// 		mutex: eraseButton.bstate || drawButton.bstate
// 	}

	Button {
		id: noteButton
		//text: "N"
		image: "note"
		anchors.right: drawButton.left
		anchors.rightMargin: 5
		width: annoBar.refLength
		height: annoBar.refLength
		onClicked: toggleState();
		mutex: eraseButton.bstate || drawButton.bstate
		onBstateChanged: {
			if(bstate){
				noteText.forceActiveFocus();
				noteText.text = "";
			}
		}
	}
	
	Button {
		id: drawButton
		//text: "draw"
		image: "pencil"
		anchors.right: eraseButton.left
		anchors.rightMargin: 5
		width: annoBar.refLength
		height: annoBar.refLength
		onClicked: toggleState();
		mutex: eraseButton.bstate || noteButton.bstate
	}
	
	Button {
		id: eraseButton
		//text: "draw"
		image: "eraser"
		anchors.right: closeButton.left
		anchors.rightMargin: 5
		width: annoBar.refLength
		height: annoBar.refLength
		onClicked: toggleState();
		mutex: drawButton.bstate || noteButton.bstate
		onBstateChanged: {
			if(bstate)
				sethist.setCursorImage("eraser_inv");
			else
				sethist.setCursorShape(Qt.ArrowCursor);
		}
	}
	
	
	Button {
		id: closeButton
		text: "X"
		anchors.right: parent.right
		anchors.rightMargin: 5
		width: annoBar.refLength
		height: annoBar.refLength
		onClicked: annoBar.close()
	}
	
	
	Grid {
		id: colorPicker
		visible: pickerButton.bstate
		anchors.top: drawButton.bottom;
		anchors.topMargin: 4
		anchors.right: drawButton.right
		rows: 4; columns: 3; spacing: 3
		
		function setCol(c){
			choosenColor = c
			colorPicker.visible = false
		}
		
		Button { color: "red"; width: annoBar.refLength;height: annoBar.refLength;onClicked: colorPicker.setCol(color) }
		Button { color: "green"; width: annoBar.refLength;height: annoBar.refLength;onClicked: colorPicker.setCol(color) }
		Button { color: "blue"; width: annoBar.refLength;height: annoBar.refLength;onClicked: colorPicker.setCol(color) }
		Button { color: "yellow"; width: annoBar.refLength;height: annoBar.refLength;onClicked: colorPicker.setCol(color) }
		Button { color: "purple"; width: annoBar.refLength;height: annoBar.refLength;onClicked: colorPicker.setCol(color) }
		Button { color: "cyan"; width: annoBar.refLength;height: annoBar.refLength;onClicked: colorPicker.setCol(color) }
		
		Button { color: "#BFBFBF"; width: annoBar.refLength;height: annoBar.refLength;onClicked: colorPicker.setCol(color) }
		Button { color: "gray"; width: annoBar.refLength;height: annoBar.refLength;onClicked: colorPicker.setCol(color) }
		Button { color: "black"; width: annoBar.refLength;height: annoBar.refLength;onClicked: colorPicker.setCol(color) }
		
	}
	
	TextInput {
		id: noteText
		echoMode: TextInput.NoEcho
		onFocusChanged: cursorVisible = false;
		onAccepted: text += "\n"
		Keys.onReleased: {
			if(event.key == Qt.Key_Escape || event.key == Qt.Key_Control)
				noteButton.state = "";
		}
	}
}
