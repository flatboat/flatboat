/* This file is part of "modern computer flatboat", a pdf viewer.
 * Copyright (C) 2012 Frank Fuhlbrück 
 * License: GPLv3 (or any later version, at your option)
 * See the file "LICENSE".
 */
import QtQuick 2.0

Rectangle {
	id: rect
	color: "#889988"
	border.color: lbl.color
	border.width: 1
	property alias text: lbl.text
	property alias rightText: lbl2.text
	property alias pointSize: lbl.font.pointSize
	property alias wrapMode: lbl.wrapMode
	property alias lineHeight: lbl.lineHeight
	property string image;
	property bool bstate: state == "active"
	property bool mutex: false
	onMutexChanged:{if(mutex) state = ""}
	signal clicked
	signal rightTextClicked
	width:  10 + 1.05 * lbl.implicitWidth
	height: 10 + lbl.implicitHeight
	radius: 4
	clip: true
	property bool centerText: true
	MouseArea {
		id : ma
		anchors.fill: parent
		onClicked: rect.clicked()
	}
	Text {
		id: lbl
		color: "#ffffff"
		text: ""
		font.pointSize: 22
		font.weight: Font.DemiBold
		style: Text.Raised
		anchors.left : parent.centerText ? undefined : parent.left;
		anchors.right : parent.centerText ? undefined : parent.right;
		anchors.margins : 5;
		anchors.verticalCenterOffset: 0
		anchors.horizontalCenterOffset: 1
		anchors.centerIn: parent.centerText ? parent : null
	}
	
	Text {
		id: lbl2
		color: "#ffffff"
		text: ""
		font.pointSize: 22
		font.weight: Font.DemiBold
		style: Text.Raised
		anchors.right: parent.right;
		anchors.margins : 5;
		anchors.verticalCenterOffset: 0
		anchors.horizontalCenterOffset: 1
		
		MouseArea {
			id : lbl2_ma
			anchors.fill: parent
			onClicked: rect.rightTextClicked()
		}
	}
	
	Image {
		id: img
		anchors.fill: parent
		fillMode: Image.PreserveAspectFit
		source: rect.image?(rect.state == "active" ? rect.image+"_inv.png":rect.image+".png"):""
		sourceSize.width: 256
        	sourceSize.height: 256
		smooth: true
		anchors.margins: 3
	}
	

	
	function toggleState(){
		if(state == "active")
			state = ""
		else
			state = "active"
	}
	
	states: [
	State {
		name: "active"
		PropertyChanges {target: rect; color: "#ffffff"; }
		PropertyChanges {target: lbl; color: "#889988"; }
	},
	State {
		name: "disabled"
		//PropertyChanges {target: rect; color: "#f5fef5"; }
		PropertyChanges {target: rect; opacity: 0.5}
		PropertyChanges {target: rect; onClicked: {}}
		
	}
	]
}
