/* This file is part of "modern computer flatboat", a pdf viewer.
 * Copyright (C) 2012,2013 Frank Fuhlbrück 
 * License: GPLv3 (or any later version, at your option)
 * See the file "LICENSE".
 */
import QtQuick 2.0

Rectangle {
	id: outerRect
	color: "transparent"
	width: 2.2*post.width
	property alias text: ipText.text
	property alias inputMask: ipText.inputMask
	property alias postText: post.text
	signal accepted
	signal focusLost
	signal focusGained
	function off(){
		ipText.focus = false;
	}
	
	Rectangle {
		id: rect
		border.width: 1
		width: parent.width - post.width;
		height: parent.height
		radius: 2
		
		TextInput{
			id: ipText
			text: ""
			width: parent.width
			font.pointSize: 17
			anchors.verticalCenter: parent.verticalCenter
			anchors.left: parent.left
			anchors.leftMargin: 4
			onAccepted: outerRect.accepted();
			onFocusChanged: {
				if(!focus)
					outerRect.focusLost();
				else
					outerRect.focusGained();
			}
			Keys.priority: Keys.AfterItem;
			Keys.onReleased: {
				switch(event.key){
					case Qt.Key_Close:
					case Qt.Key_Escape:
						ipText.focus = false;
						break;
				}
				event.accepted = true;
			}
		}
	}
	
	Text{
		id: post
		width: implicitWidth
		font.pointSize: ipText.font.pointSize
		anchors.left: rect.right
		anchors.leftMargin: 5
		anchors.verticalCenter: parent.verticalCenter
	}
	
	states: [
	State {
		name: "disabled"
		//PropertyChanges {target: rect; color: "#f5fef5"; }
		PropertyChanges {target: rect; opacity: 0.5}
		PropertyChanges {target: ipText; readOnly : true}
		
	}
	]
}
