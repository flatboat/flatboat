/* This file is part of "modern computer flatboat", a pdf viewer.
 * Copyright (C) 2012 Frank Fuhlbrück 
 * License: GPLv3 (or any later version, at your option)
 * See the file "LICENSE".
 */
import QtQuick 2.0

Rectangle {
	id: tocView
	z: 2
	width: parent.width*0.95
	height: parent.height*0.95
	property variant outline;
	radius: 10
	clip: true
	anchors.centerIn: parent
	border.width: 4
	visible: false
	focus: visible
	onOutlineChanged:  {modl.clear();fillList(outline,0)}
	onVisibleChanged: if(!visible) parent.focus = true;
	function close(){
		parent.state = "";
	}
	function fillList(list,l){
		for(var j=0;j<list.length;j++){
			var o = list[j];
			o.lvl = l;
			modl.append(o);
			if(o.sub)
				fillList(o.sub,l+1);
		}
	}
	signal requestPageJump(int pn);
	

	
	Text{
		id: headline
		anchors.top : parent.top
		anchors.left: parent.left;
		anchors.margins : 5;
		width: parent.width
		clip: false
		wrapMode:Text.Wrap
		text: "table of contents"
		font.pointSize: 22
	}
	Text{
		id: docName
		anchors.top : headline.bottom
		anchors.left: parent.left;
		anchors.margins : 5;
		width: parent.width
		clip: false
		textFormat: Text.RichText
		wrapMode:Text.Wrap
		text: (sethist.curDoc.title?sethist.curDoc.title:sethist.curDoc.filepath) +
		(sethist.curDoc.author?("<br/>by " +sethist.curDoc.author):"")
		font.pointSize: 14
	}
	Button {
		id: close 
		anchors.top : parent.top
		anchors.right: parent.right;
		anchors.margins : 5;
		text: "x"
		onClicked: {tocView.close();}
	}
	ListView{
		id: pagelist
		height: 0.95*tocView.height; 
		flickableDirection: Flickable.VerticalFlick
		boundsBehavior: Flickable.StopAtBounds
		snapMode: ListView.SnapToItem
		

		anchors.top : docName.bottom;
		anchors.left: parent.left;
		anchors.right: parent.right;
		anchors.margins : 5;
		anchors.bottom:parent.bottom;
		clip: true
		model: ListModel{
			id:modl	
		}
		
		delegate: Button{
			text: title?title:""
			rightText: page
			wrapMode: Text.Wrap
			centerText: false
			x: 10*lvl
			width: parent ? parent.width-x : 0
// 			anchors.left : parent.left
// 			anchors.right: parent.right
			pointSize: mainRec.refPtSize * 2/3;
			onClicked: {
				tocView.requestPageJump(page);
				tocView.close();
			}
		}
		

	}

}
