/* This file is part of "modern computer flatboat", a pdf viewer.
 * Copyright (C) 2012 Frank Fuhlbrück 
 * License: GPLv3 (or any later version, at your option)
 * See the file "LICENSE".
 */
#ifndef QPDFIMAGE_H
#define QPDFIMAGE_H

#include<QVariant>
#include<QImage>
#if QT_VERSION < 0x050000
#include <QDeclarativeItem>
#define ITEMC QDeclarativeItem
#define ITEMCI QDeclarativeItem
#define ADD_PAINT_PAR , const QStyleOptionGraphicsItem *, QWidget *
#define MOUSE_EVENTC QGraphicsSceneMouseEvent
#else
#include <QQuickPaintedItem>
#define ITEMC QQuickItem
#define ITEMCI QQuickPaintedItem
#define ADD_PAINT_PAR 
#define MOUSE_EVENTC QMouseEvent
#endif



#include <QTimer>
#include <QDateTime>
extern "C" {
	#include <fitz.h>
	#include <mupdf.h>
	#include <mupdf-internal.h>
}

class PdfLink : public QObject {
	Q_OBJECT
	
public:
	explicit PdfLink(QObject *parent = 0);
	PdfLink(fz_link* fzl);
	PdfLink(const PdfLink&);
	Q_PROPERTY(QRect position READ position)
	Q_PROPERTY(int type READ type)//0: invalid, 1:PDF page 2: URI
	Q_PROPERTY(QVariant destination READ destination)
	
	QRect position();
	int type();
	QVariant destination();
	
	PdfLink& operator=(const PdfLink&);

private:
	QRect position_prv;
	int type_prv;
	QVariant destination_prv;
};
Q_DECLARE_METATYPE (PdfLink)
Q_DECLARE_METATYPE (QRect*)

class PdfAnnotation {
public:
	enum PdfAnnoState {UNCHANGED, DELETED, MODIFIED, NEW};

	QString subtype;
	virtual QRectF rect();//PDF coordinates, up is positive
	virtual void setRect(QRectF);
	QDateTime date;
	QColor col;
	QString contents;
	PdfAnnoState state;
	
	static PdfAnnotation* fromPdfObj(pdf_obj *anno);
	virtual pdf_obj* toPdfObj(fz_context *context);
	
protected:
	PdfAnnotation(QString subtype);
	PdfAnnotation(const PdfAnnotation &copy);
	PdfAnnotation(pdf_obj *anno);
	PdfAnnotation& operator=(PdfAnnotation &copy);
	
	pdf_obj* originalPdfObj;
	
	QRectF rect_prv;
};

class PdfInkAnnotation : public PdfAnnotation {
public:
	
	PdfInkAnnotation();
	PdfInkAnnotation(pdf_obj *anno);
	PdfInkAnnotation(const PdfInkAnnotation &copy);
	PdfInkAnnotation& operator=(PdfInkAnnotation &copy);

	QPainterPath path;
	virtual QRectF rect();//PDF coordinates, up is positive
	
	virtual pdf_obj* toPdfObj(fz_context *context);
};

class PdfFreeTextAnnotation : public PdfAnnotation {
public:
	
	PdfFreeTextAnnotation();
	PdfFreeTextAnnotation(pdf_obj *anno);
	PdfFreeTextAnnotation(const PdfFreeTextAnnotation &copy);
	PdfFreeTextAnnotation& operator=(PdfFreeTextAnnotation &copy);

	QString defApp;
	QString defStyle;//since PDF 1.5
	
	virtual pdf_obj* toPdfObj(fz_context *context);
};


class PdfTextAnnotation : public PdfAnnotation {
public:
	
	PdfTextAnnotation();
	PdfTextAnnotation(pdf_obj *anno);
	PdfTextAnnotation(const PdfTextAnnotation &copy);
	PdfTextAnnotation& operator=(PdfTextAnnotation &copy);
	
	virtual pdf_obj* toPdfObj(fz_context *context);
	
	bool open;
};

class QPdfImage : public ITEMCI
{
	Q_OBJECT
public:
	explicit QPdfImage(ITEMC *parent = 0);
	~QPdfImage();
	Q_PROPERTY(QString filepath READ filepath WRITE loadFile())
	Q_PROPERTY(bool textMode READ textMode WRITE setTextMode NOTIFY textModeChanged)
	Q_PROPERTY(int textModeFontSize READ textModeFontSize WRITE setTextModeFontSize NOTIFY textModeFontSizeChanged)
	Q_PROPERTY(bool grainyMode READ grainyMode WRITE setGrainyMode NOTIFY grainyModeChanged)
	Q_PROPERTY(int pagenum READ pagenum WRITE gotoPage NOTIFY pagenumChanged)
	Q_PROPERTY(int regionIdx READ regionIdx WRITE setRegionIdx NOTIFY regionIdxChanged)
	Q_PROPERTY(int totalPages READ totalPages NOTIFY totalPagesChanged)
	Q_PROPERTY(int zoom READ zoom WRITE setZoom NOTIFY zoomChanged)
	Q_PROPERTY(bool stretched READ stretched NOTIFY stretchedChanged)
	//pixel size at 72dpi(see fitz.h):
	Q_PROPERTY(int originalWidth READ originalWidth NOTIFY originalWidthChanged)
	Q_PROPERTY(int originalHeight READ originalHeight NOTIFY originalHeightChanged)
	Q_PROPERTY(QString highlightPhrase READ highlightPhrase WRITE setHighlightPhrase)
	Q_PROPERTY(QList<PdfLink> links READ links)
	Q_PROPERTY(bool linkHints READ linkHints WRITE setLinkHints NOTIFY linkHintsChanged)
	Q_PROPERTY(int pagenum READ pagenum WRITE gotoPage NOTIFY pagenumChanged)
	Q_PROPERTY(bool caseSensitive READ caseSensitive WRITE setCaseSensitive)
	Q_PROPERTY(QRect hitRect READ hitRect NOTIFY hitRectChanged)
	Q_PROPERTY(bool drawMode READ drawMode WRITE setDrawMode NOTIFY drawModeChanged)
	Q_PROPERTY(bool eraseMode READ eraseMode WRITE setEraseMode NOTIFY eraseModeChanged)
	Q_PROPERTY(bool noteMode READ noteMode WRITE setNoteMode NOTIFY noteModeChanged)
	Q_PROPERTY(bool modified READ modified)
	Q_PROPERTY(QColor drawColor READ drawColor WRITE setDrawColor NOTIFY drawColorChanged)
	
	Q_PROPERTY(QString title READ title NOTIFY titleChanged)
	Q_PROPERTY(QString author READ author NOTIFY authorChanged)
	Q_PROPERTY(QVariantList outline READ outline NOTIFY outlineChanged)
	
	
	QString filepath();
	bool textMode();
	int textModeFontSize();
	bool grainyMode();
	int totalPages();
	int pagenum();
	int regionIdx();
	int region();
	int zoom();
	bool stretched();
	int originalWidth();
	int originalHeight();
	QString highlightPhrase();
	QList<PdfLink> links();
	bool linkHints();
	bool caseSensitive();
	QRect hitRect();
	bool drawMode();
	bool eraseMode();
	bool noteMode();
	bool modified();
	QColor drawColor();
	QString title();
	QString author();
	QVariantList outline();
	
	 
	
	void paint(QPainter *painter ADD_PAINT_PAR);
	void mousePressEvent (MOUSE_EVENTC  *event);
	void mouseReleaseEvent (MOUSE_EVENTC *);
	void mouseDoubleClickEvent (MOUSE_EVENTC *);
	void mouseMoveEvent (MOUSE_EVENTC  *event);
	
	
signals:
	void regionIdxChanged(int idx);
	void pagenumChanged(int pn);
	void renderingFinished();
	void totalPagesChanged(int tp);
	void zoomChanged(int zm);
	void stretchedChanged();
	void originalWidthChanged();
	void originalHeightChanged();
	void textModeChanged(bool tm);
	void textModeFontSizeChanged();
	void grainyModeChanged(bool gm);
	void clickForward();//forward clicks, that don't have a meaning for qpdfimage
	void allPressForward();//forward all mouse down events, even those, that may have a meaning for QPdfImage
	void hitRectChanged();
	void linkSelected(QVariant destination);
	void linkHintsChanged(bool lh);
	void drawModeChanged(bool dm);
	void eraseModeChanged(bool em);
	void noteModeChanged(bool ftm);
	void newNote();
	void drawColorChanged();
	void titleChanged(QString t);
	void authorChanged(QString a);
	void outlineChanged();
	
public slots:
	bool loadFile(QString filename);
	void setTextMode(bool tm);
	void setTextModeFontSize(int fs);
	void setGrainyMode(bool gm);
	bool gotoPage(int pagenum,bool render=true);
	void render();
	void askForRendering();
	bool setZoom(int percent,bool render=true);
	bool stretch(int width,int height);
	void setHighlightPhrase(QString phrase);
	void setLinkHints(bool lh);
	void setCaseSensitive(bool cs);
	bool searchNext(QString str);
	bool searchPrev(QString str);
	QRect prevRegion(bool render=true);
	QRect curRegion();//no side effekt, therefore no slot
	QRect nextRegion(bool render=true);
	QRect setRegionIdx(int idx);
	void stopClickTimer();
	void setDrawMode(bool dm);
	void setEraseMode(bool em);
	void setNoteMode(bool ftm);
	void updateAnnoText(QString atext);
	void setDrawColor(QColor cl);
	void writeCurrentFile();

	
private:
	QString filepath_prv;
	QImage *image;
	QString lastSearchPhrase;
	bool lastSearchCaseSensitive;
	QList<QRect> regions;
	int curRegionIdx;
	QStringList textDocument;
	QList<QRect> highlight;
	QList<int> hitCount;
	int hitOnPage;
	fz_context *context;
	fz_document *document;
	fz_pixmap *pixmap;
	fz_page *page;
	pdf_obj *pageobj;
	fz_text_page *text_page;
	bool textMode_prv;
	int textModeFontSize_prv;
	bool grainyMode_prv;
	QTimer renderTimer;
	bool isLoading;
	QImage loadImage;
	int pagenum_prv;
	int totalpages_prv;
	int zoom_prv;
	int zoom_y_prv;
	int originalWidth_prv;
	int originalHeight_prv;
	QString highlightPhrase_prv;
	QList<PdfLink> links_prv;
	bool linkHints_prv;
	bool caseSensitive_prv;
	QTimer clickTimer;
	bool eraseMode_prv;
	bool drawMode_prv;
	bool noteMode_prv;
	QImage noteImage;
	QColor drawColor_prv;
	QList<PdfAnnotation*> annos;
	int pageModifications;
	bool fileModified;
	QString title_prv;
	QString author_prv;
	QVariantList outline_prv;
	
	QStringList document2stringList();
	void saveCloseCurrentFile();
	void writeAnnotationsToPage();
	void readAnnotationsFromPage();
	
};
#endif // QPDFIMAGE_H
