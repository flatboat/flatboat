/* This file is part of "modern computer flatboat", a pdf viewer.
 * Copyright (C) 2012 Frank Fuhlbrück 
 * License: GPLv3 (or any later version, at your option)
 * See the file "LICENSE".
 */
#ifndef SETHIST_H
#define SETHIST_H

#include<QSettings>
#include<QVariant>

#if QT_VERSION < 0x050000
#define WINDOWC QWidget
#else
#include <QWindow>
#define WINDOWC QWindow
#endif

//a class for settings and history
class SetHist : public QObject {
	Q_OBJECT
public:
	explicit SetHist(QObject* parent = 0);
	SetHist(const SetHist&);
	Q_PROPERTY(QVariantList documents READ documents())
	Q_PROPERTY(QVariantMap curDoc READ curDoc() WRITE setCurDoc() NOTIFY curDocChanged())
	
	QVariantList documents();
	QVariantMap curDoc();
	void setWindow(WINDOWC *wdw);
	
	
signals:
	void curDocChanged();
public slots:
	void newDocument(QVariantMap doc);
	bool chooseFile(QString dir);
	void setCurDoc(QVariantMap doc);
	void clearDocuments();
	void removeDocument(int i);
	void setCursorImage(QString img);
	void setCursorShape(int shape);
	void setValue (QString name, const QVariant &value);
	QVariant value(QString name);
	static void setUpdateMode(int mode);
	static void log(QVariant str);
	static QString platform();
	static QString displayType();
	
private:
	QSettings qs;
	QVariantList documents_prv;
	WINDOWC *window;
};
Q_DECLARE_METATYPE (SetHist)
#endif // SETHIST_H
