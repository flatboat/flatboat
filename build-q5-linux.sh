find . -name *.qml -exec  sed -i 's/QtQuick 1.1/QtQuick 2.0/' {} \;
cd `dirname $0`
mkdir -p ../flatboat-build-q5-linux
cd ../flatboat-build-q5-linux
/usr/lib/x86_64-linux-gnu/qt5/bin/qmake CONFIG+=unix-local-debug ../flatboat/flatboat.pro
make
find ../flatboat -name *.qml -exec  sed -i 's/QtQuick 1.1/QtQuick 2.0/' {} \;
