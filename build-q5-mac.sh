find . -name *.qml -exec  sed -i "" 's/QtQuick 1.1/QtQuick 2.0/' {} \;
cd `dirname $0`
mkdir -p ../flatboat-build-q5-mac
cd ../flatboat-build-q5-mac
qmake5 ../flatboat/flatboat.pro
make
find ../flatboat/ -name *.qml -exec  sed -i "" 's/QtQuick 2.0/QtQuick 1.1/' {} \;
cp ../flatboat/Info.plist flatboat.app/Contents/Info.plist
if [ -n "$1" ]; then
chmod -R u+w flatboat.app
macdeployqt5 flatboat.app -no-plugins -verbose=3
chmod -R u+w flatboat.app
for name in QtCore QtQml QtWidgets QtGui QtQuick QtNetwork QtV8  
do
	install_name_tool -id @executable_path/../Frameworks/${name}.framework/Versions/5/${name} flatboat.app/Contents/Frameworks/${name}.framework/Versions/4/${name}
 	install_name_tool -change /opt/Qt5SDK/5.0.0/clang_64/lib/${name}.framework/Versions/5/${name} @executable_path/../Frameworks/${name}.framework/Versions/4/${name} flatboat.app/Contents/MacOS/flatboat
	for iname in QtCore QtQml QtWidgets QtGui QtQuick QtNetwork QtV8
	do
		install_name_tool -change /opt/Qt5SDK/5.0.0/clang_64/lib/${name}.framework/Versions/5/${name} @executable_path/../Frameworks/${name}.framework/Versions/4/${name} flatboat.app/Contents/Frameworks/${iname}.framework/Versions/5/${iname}
	done
done
fi
