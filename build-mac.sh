cd `dirname $0`
mkdir -p ../flatboat-build-mac
cd ../flatboat-build-mac
qmake ../flatboat/flatboat.pro
make
cp ../flatboat/Info.plist flatboat.app/Contents/Info.plist
if [ -n "$1" ]; then
chmod -R u+w flatboat.app
macdeployqt flatboat.app -no-plugins -verbose=3
chmod -R u+w flatboat.app
#mkdir -p flatboat.app/Contents/Frameworks/
#for name in QtDeclarative QtScript QtCore QtSvg QtGui QtSql QtXmlPatterns QtNetwork
#do
#	cp -R /opt/local/Library/Frameworks/${name}.framework/  flatboat.app/Contents/Frameworks/${name}.framework/
#done
for name in QtDeclarative QtScript QtCore QtSvg QtGui QtSql QtXmlPatterns QtNetwork
do
	install_name_tool -id @executable_path/../Frameworks/${name}.framework/Versions/4/${name} flatboat.app/Contents/Frameworks/${name}.framework/Versions/4/${name}
 	install_name_tool -change /opt/local/Library/Frameworks/${name}.framework/Versions/4/${name} @executable_path/../Frameworks/${name}.framework/Versions/4/${name} flatboat.app/Contents/MacOS/flatboat
	for iname in QtDeclarative QtScript QtCore QtSvg QtGui QtSql QtXmlPatterns QtNetwork
	do
		install_name_tool -change /opt/local/Library/Frameworks/${name}.framework/Versions/4/${name} @executable_path/../Frameworks/${name}.framework/Versions/4/${name} flatboat.app/Contents/Frameworks/${iname}.framework/Versions/4/${iname}
	done
done
fi
