/* This file is part of "modern computer flatboat", a pdf viewer.
 * Copyright (C) 2012,2013 Frank Fuhlbrück 
 * License: GPLv3 (or any later version, at your option)
 * See the file "LICENSE".
 */

#include "sethist.h"
#if QT_VERSION < 0x050000
#include <QtGui/QFileDialog>
#else
#include <QtWidgets/QFileDialog>
#endif
#include <QCoreApplication>


SetHist::SetHist(QObject *parent) :
QObject(parent), qs("flatboat","flatboat"), window(NULL) {
#ifdef Q_OS_ANDROID
	qs.setPath(QSettings::NativeFormat, QSettings::UserScope,
		QCoreApplication::applicationDirPath());
#endif //Q_OS_ANDROID
	documents_prv = qs.value("documentHistory",QVariantList()).toList();
}

SetHist::SetHist(const SetHist& copy) : QObject(copy.parent()), qs("flatboat","flatboat") {
	documents_prv = qs.value("documentHistory",QVariantList()).toList();
	window = copy.window;
}

void SetHist::setWindow(WINDOWC* wdw){
	window = wdw;
}


void SetHist::newDocument(QVariantMap doc){
	int i=0;
	for(;i<documents_prv.length();i++)
		if(doc["filepath"].toString() == documents_prv[i].toMap()["filepath"].toString())
			break;
	
	if(i<documents_prv.length())//in list
		documents_prv.move(i,0);
	else 
		documents_prv.prepend(doc);
	qs.setValue("documentHistory",documents_prv);
}

QVariantList SetHist::documents(){
	return documents_prv;
}

QVariantMap SetHist::curDoc(){
	if(documents_prv.length() > 0)
		return documents_prv[0].toMap();
	else
		return QVariantMap();
}

void SetHist::setCurDoc(QVariantMap doc){
	documents_prv[0] = doc;
	qs.setValue("documentHistory",documents_prv);
	emit curDocChanged();
}

void SetHist::clearDocuments(){
	QVariantMap doc = documents_prv[0].toMap();
	documents_prv.clear();
	documents_prv.append(doc);
	qs.setValue("documentHistory",documents_prv);
}

void SetHist::removeDocument(int i){
	documents_prv.removeAt(i);
}

#ifdef Q_OS_ANDROID
#define DEFPATH "/mnt/sdcard"
#else
#define DEFPATH QDir::homePath()
#endif //Q_OS_ANDROID
bool SetHist::chooseFile(QString dir){
	QVariantMap doc;
	if(dir == ""){
		dir = DEFPATH;
		if(documents_prv.length() > 0){
			QFileInfo lf(documents_prv[0].toMap()["filepath"].toString());
			dir = lf.dir().exists() ? lf.dir().path() : DEFPATH;
		} 
	}
	doc["filepath"] = QFileDialog::getOpenFileName(0, "Select PDF file", dir, "PDF (*.pdf)");
	if(doc["filepath"].toString().isNull())
		return false;
	newDocument(doc);
	return true;
}


void SetHist::setCursorImage(QString img){
	window->setCursor(QCursor(QPixmap(":/"+img+".png").scaled(32,32)));
}

void SetHist::setCursorShape(int shape){
	window->setCursor((Qt::CursorShape) shape);
}

void SetHist::setValue (QString name, const QVariant &value){
	qs.setValue(name,value); 
}

QVariant SetHist::value(QString name){
	return qs.value(name);
}

#ifdef Q_OS_ANDROID
#include <jni.h>
extern JavaVM* jvm;
extern jobject qtactivity;

//for PRS T1, doesn't work (yet)
void SetHist::setUpdateMode(int mode){
	JNIEnv *jenv;
	jvm->AttachCurrentThread(&jenv, NULL);
	
	
// 	jclass ctext_cls = jenv->FindClass("android/app/Application");
// 	if(jenv->ExceptionCheck()){log("ctext_cls went wrong");jenv->ExceptionClear();return;}
// 	jmethodID ctext_ctr = jenv->GetMethodID(ctext_cls,"<init>", "()V");
// 	if(jenv->ExceptionCheck()){log("ctext_ctr went wrong");jenv->ExceptionClear();return;}
// 	jobject ctext = jenv->NewObject(ctext_cls, ctext_ctr);
// 	if(jenv->ExceptionCheck()){log("ctext went wrong");jenv->ExceptionClear();return;}

	jclass lview_cls = jenv->FindClass("android/widget/ListView");
	if(jenv->ExceptionCheck()){log("lview_cls went wrong");jenv->ExceptionClear();return;}
	jmethodID lview_ctr = jenv->GetMethodID(lview_cls,"<init>", "(Landroid/content/Context;)V");
	if(jenv->ExceptionCheck()){log("lview_ctr went wrong");jenv->ExceptionClear();return;}
	jmethodID invMethod = jenv->GetMethodID(lview_cls, "invalidate","(I)V");
	if(jenv->ExceptionCheck()){log("invMethod went wrong");jenv->ExceptionClear();return;}
	jobject lview = jenv->NewObject(lview_cls, lview_ctr, qtactivity);
	//if(jenv->ExceptionCheck()){log("lview went wrong");jenv->ExceptionClear();return;}
	jenv->ExceptionClear();
	log((qlonglong)lview);
		
	jenv->CallVoidMethod(lview,invMethod,mode);
}
#else
void SetHist::setUpdateMode(int){}
#endif //Q_OS_ANDROID

#ifdef Q_OS_ANDROID
#include <android/log.h>
void SetHist::log(QVariant what){
	__android_log_print(ANDROID_LOG_WARN,"flatboat", what.toString().toUtf8().data());
}
#else
#include <QDebug>
void SetHist::log(QVariant what){
	qDebug() << what.toString();
}
#endif //Q_OS_ANDROID

QString SetHist::platform(){
#ifdef Q_OS_ANDROID
	return "android";
#elif defined Q_WS_MAEMO_5
	return "maemo5";
#elif defined Q_OS_LINUX
	return "linux";
#elif defined Q_OS_MAC
	return "macx";
#elif defined Q_OS_WIN32
	return "win32";
#else
	return "default";
#endif
}

QString SetHist::displayType(){
#ifdef EINK_DISPLAY
	return "eink";
#else
	return "normal";
#endif
}
