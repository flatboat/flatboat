#!/bin/bash
if [ ! -d ../mupdf-git ]; then
	exit
fi
cd `dirname $0`
cd ..
git clone git://git.ghostscript.com/mupdf.git mupdf-git
#tested commit:
cd mupdf-git
git checkout 4d36e9613da5190aba2c0c6c8281e5d35cf9758c
wget http://www.mupdf.com/download/mupdf-thirdparty.zip
unzip mupdf-thirdparty.zip
make
mv build/debug build/debug-linux

