#$1 = debug/release
NECESSITAS_ROOT=/opt/NecessitasQtSDK
cd `dirname $0`
mkdir -p ../flatboat-build-eink-android
cd ../flatboat-build-eink-android
export ANDROID_NDK_ROOT=${NECESSITAS_ROOT}/android-ndk/
${NECESSITAS_ROOT}/Android/Qt/482/armeabi/bin/qmake CONFIG+=eink ../flatboat/flatboat.pro
make
INSTALL_ROOT=../flatboat/android/ make install
cd ../flatboat/android/
ant $1 
mv bin/flatboat-$1.apk bin/flatboat-eink-$1.apk

