/* This file is part of "modern computer flatboat", a pdf viewer.
 * Copyright (C) 2012,2013 Frank Fuhlbrück 
 * License: GPLv3 (or any later version, at your option)
 * See the file "LICENSE".
 */

#include "qpdfimage.h"
#include <QPainter>

#if QT_VERSION < 0x050000
#include <QtGui/QLabel>
#define TOPOINT(A) A.toPoint()
#define SET_HAS_CONTENTS_FLAG setFlag (ItemHasNoContents,false);
#else
#include <QtWidgets/QLabel>
#define TOPOINT(A) A
#define SET_HAS_CONTENTS_FLAG setFlag(ItemHasContents, true);
#endif

#include <QFile>

#include <QPaintEngine>
#include <QDebug>
#if QT_VERSION < 0x050000
#include <QGraphicsSceneMouseEvent>
#else
#include <QMouseEvent>
#endif

//#include <QtConcurrentRun>



typedef unsigned char t_rgba[4];
#define swap(x,y) { x = x + y; y = x - y; x = x - y; }
void rgba2bgra(unsigned char *dat, int width, int height) {
	t_rgba *data = (t_rgba*) dat;
	for (int i = 0; i < width*height; i++) 
		swap(data[i][0],data[i][2])
}
#undef swap
void invertSamples(unsigned char *dat, int width, int height) {
	t_rgba *data = (t_rgba*) dat;
	for (int i = 0; i < width*height; i++) {
		data[i][0] = 255 - data[i][0];
		data[i][1] = 255 - data[i][1];
		data[i][2] = 255 - data[i][2];
	}       
}

static QRect fz_rectToQRect(fz_rect rect){
	fz_bbox b = fz_bbox_covering_rect(rect);
	return QRect(b.x0,b.y0,b.x1-b.x0,b.y1-b.y0);
}


static QString textChar_p2string(fz_text_char *text,int len,fz_rect** rectsOfChars=NULL){
	//MuPDF's internal format seems to be ucs4, so double conversion(->utf8->utf16) is unnecessary:
	//     int reallen = 0;
	//     char utf8[len*4];
	//     char chars[4];
	fz_rect *rects = new fz_rect[len];
	unsigned int ucs4[len];
	for(int i=0;i<len;i++){
		//         int lchars = fz_runetochar(chars, text[i].c);
		//         for(int j = 0; j < lchars; j++)
		//             utf8[reallen+j] = chars[j];
		ucs4[i] = text[i].c;
		rects[i] = text[i].bbox;
		//         reallen += lchars;
	}
	if(rectsOfChars)
		*rectsOfChars = rects;
	return QString::fromUcs4(ucs4,len);
}

static QString page2string(fz_text_page *page){
	fz_text_block *block;
	fz_text_line *line;
	fz_text_span *span;
	QString str("");
	for (block = page->blocks; block < page->blocks + page->len; block++){
		for (line = block->lines; line < block->lines + block->len; line++){
			for (span = line->spans; span < line->spans + line->len; span++)
				str += textChar_p2string(span->text,span->len) + "\n";// pseudo-newline
		}
	}
	return str;
}

static QString adjustPageString(QString str, int minLineLength){
	QStringList lines = str.split("\n");
	if(minLineLength == -1){//auto
		for(int i=0;i<lines.length();i++)
			minLineLength = std::max(lines[i].length(),minLineLength);
		minLineLength /= 2;
	}
	QString buf;
	QString res;
	for(int i=0;i<lines.length();i++){
		buf += lines[i];
		if(buf.length() >= minLineLength || i==lines.length()-1 || lines[i+1].length() >= minLineLength){
			res += buf +"\n";
			buf = "";
		}
	}
	return res;
}

static QString block2string(fz_text_block *block, bool addNewLines){
	fz_text_line *line;
	fz_text_span *span;
	QString str("");
	for (line = block->lines; line < block->lines + block->len; line++){
		for (span = line->spans; span < line->spans + line->len; span++)
			str += textChar_p2string(span->text,span->len) + (addNewLines?"\n":"");// pseudo-newline
	}
	return str;
}

static QList<QRect> searchInPage(fz_text_page *page,QString search,bool caseSensitive){
	fz_text_block *block;
	fz_text_line *line;
	fz_text_span *span;
	fz_rect* rects;
	QList<QRect> highlight;
	QString str;
	int fpos;
	int lpos;
	QRect uni;
	if(search == "")
		return highlight;
	for (block = page->blocks; block < page->blocks + page->len; block++){
		for (line = block->lines; line < block->lines + block->len; line++){
			for (span = line->spans; span < line->spans + line->len; span++){
				str = textChar_p2string(span->text,span->len,&rects);
				fpos = -1;
				while((fpos = str.indexOf(search,fpos+1,(Qt::CaseSensitivity)caseSensitive)) > -1){
					lpos = fpos + search.length();
					uni = fz_rectToQRect(rects[fpos]);
					for(int i=fpos+1;i<lpos;i++)
						uni |= fz_rectToQRect(rects[i]);
					highlight += uni;
				}
			}
		}
	}
	return highlight;	
}

static inline int commonWidth(QRect a,QRect b){
	int x0 = std::max(a.left(),b.left());
	int x1 = std::min(a.right(),b.right())+1;//+1: see Qt doc (right())
	return std::max(x1 - x0,0);	
}

static inline uint qHash(const QRect & r){
	return qHash(QString("%1,%2,%3,%4").arg(r.x()).arg(r.y()).arg(r.width()).arg(r.height()));
}

static inline bool operator< ( const QRect & r1, const QRect & r2 ){
	return r1.left() < r2.left();
}

static QList<QRect> textAreas(fz_text_page *page,int zoom){
	fz_text_block *block;
	QList<QRect> textAreas;
	QRect uni;
	QRect nextR;
	for (block = page->blocks; block < page->blocks + page->len; block++){
		nextR = fz_rectToQRect(block->bbox);
		if(block2string(block,false).contains(QRegExp("^\\s*[0-9ivxlcdmIVXLCDM]*\\s*$")))
			continue;//page number?
		if(commonWidth(uni,nextR) < 0.3*std::min(uni.width(),nextR.width())){//new column
			textAreas += uni;
			uni = nextR;
		} else //still the same column
			uni |= nextR;
	}
	if(uni.isValid())
		textAreas += uni;
	
	for(int i=0;i<textAreas.length();i++){
		for(int j=0;j<textAreas.length();j++){
			if(i!=j && commonWidth(textAreas[i],textAreas[j]) > 0.8*std::min(textAreas[i].width(),textAreas[j].width())){
				textAreas[j] |= textAreas[i];
			}
		}
	}
	
	//page numbers:
	for(int i=0;i<textAreas.length();)
		if(textAreas[i].width() < 30*zoom/100)
			textAreas.removeAt(i);
		else 
			i++;
	
	if(textAreas.isEmpty())
		textAreas += fz_rectToQRect(page->mediabox);
	textAreas = textAreas.toSet().toList();
	qSort(textAreas);
	return textAreas;
}

QVariantList outlineToVariantList(fz_outline* outline);
QVariantMap outlineToVariantMap(fz_outline* outline){
	QVariantMap outlMap;
	fz_link_dest* fzd = &(outline->dest);
	switch(fzd->kind){
		case FZ_LINK_GOTO:
		case FZ_LINK_GOTOR://PDF-Link
			outlMap["page"] =  fzd->ld.gotor.page+1;//starts with 0
			break;
		default:
			outlMap["page"] = 0;
	}
	if(outline->title)
		outlMap["title"] = QString(outline->title);
	if(outline->down){
		outlMap["sub"] = outlineToVariantList(outline->down);
	}
	return outlMap;
}

QVariantList outlineToVariantList(fz_outline* outline){
	QVariantList outlList;
	while(outline != NULL){
		outlList.append(outlineToVariantMap(outline));
		outline = outline->next;
	}
	return outlList;
}

QRectF pdfRectToRectF(pdf_obj* pdf_rect){
	return QRectF (pdf_to_real(pdf_array_get(pdf_rect,0)),
		pdf_to_real(pdf_array_get(pdf_rect,1)),
		pdf_to_real(pdf_array_get(pdf_rect,2))-
			pdf_to_real(pdf_array_get(pdf_rect,0)),
		pdf_to_real(pdf_array_get(pdf_rect,3))-
			pdf_to_real(pdf_array_get(pdf_rect,01))
	);
}

//uses PDF coordinate system, i.e. up is positive
//don't use bottomLeft etc., they are misleading!
pdf_obj* rectFToPdfRect(QRectF rect,fz_context* context){
	pdf_obj* ret = pdf_new_array(context, 4/*int initialcap*/);
	pdf_array_push(ret, pdf_new_real(context,rect.x()));
	pdf_array_push(ret, pdf_new_real(context,rect.y()));
	pdf_array_push(ret, pdf_new_real(context,rect.x()+rect.width()));
	pdf_array_push(ret, pdf_new_real(context,rect.y()+rect.height()));
	
	return ret;
}


PdfAnnotation::PdfAnnotation(QString st): subtype(st), state(NEW), originalPdfObj(NULL) {}


PdfAnnotation::PdfAnnotation(pdf_obj *anno):  state(UNCHANGED)  {
	originalPdfObj = anno;
	pdf_obj* acol = pdf_dict_gets(anno,const_cast<char*>("C"));
	if(acol && pdf_array_len(acol) == 3){//RGB
		col.setRgbF(pdf_to_real(pdf_array_get(acol,0)),
			pdf_to_real(pdf_array_get(acol,1)),
			pdf_to_real(pdf_array_get(acol,2)));
	} else if(acol && pdf_array_len(acol) == 4){//CMYK
		col.setCmykF (pdf_to_real(pdf_array_get(acol,0)),
			pdf_to_real(pdf_array_get(acol,1)),
			pdf_to_real(pdf_array_get(acol,2)),
			pdf_to_real(pdf_array_get(acol,3)));
	} else if (acol && pdf_array_len(acol) == 1)//Gray
		col.setHslF(0,0,pdf_to_real(pdf_array_get(acol,0)));
	else
		col.setAlpha(0);//or col.setHsl(0,0,0)? transparent
	contents = pdf_to_str_buf(pdf_dict_gets(anno,const_cast<char*>("Contents")));
	rect_prv = pdfRectToRectF(pdf_dict_gets(anno,const_cast<char*>("Rect")));
}

PdfAnnotation::PdfAnnotation(const PdfAnnotation &copy) {
	subtype = copy.subtype;
	rect_prv = copy.rect_prv;
	date = copy.date;
	col = copy.col;
	contents = copy.contents;
	originalPdfObj = copy.originalPdfObj;
	state = copy.state;
}

PdfAnnotation& PdfAnnotation::operator=(PdfAnnotation &copy){
	subtype = copy.subtype;
	rect_prv = copy.rect_prv;
	date = copy.date;
	col = copy.col;
	contents = copy.contents;
	originalPdfObj = copy.originalPdfObj;
	state = copy.state;
	return *this;
}


pdf_obj* PdfAnnotation::toPdfObj(fz_context *context){
	if(state == UNCHANGED)
		return originalPdfObj;
	pdf_obj* ret = originalPdfObj ? originalPdfObj : pdf_new_dict(context, 5/*int initialcap*/);
	pdf_dict_puts(ret,const_cast<char*>("Subtype"), pdf_new_name(context,subtype.toLatin1().data()));
	pdf_dict_puts(ret,const_cast<char*>("Contents"), pdf_new_string(context, contents.toLatin1().data(), contents.toLatin1().length()));
	
	pdf_dict_puts(ret, const_cast<char*>("Rect"), rectFToPdfRect(rect(),context));
	if(date.isValid()){
		QString datestring = "D:" + date.toString("yyyyMMddhhmmss");
// 		QDateTime utc = date.toUTC();
// 		utc.setTimeSpec(Qt::LocalTime);//make them comparable
// 		int hoffset = utc.secsTo(date) / 3600;
// 		int moffset = (utc.secsTo(date) % 3600) /60;
//  		datestring += (hoffset+moffset > 0?"+":"") + QString("%1'%2").arg(hoffset).arg(moffset);
		pdf_dict_puts(ret,const_cast<char*>("M"), pdf_new_string(context, datestring.toLatin1().data(), datestring.toLatin1().length()));
	}
	
	if(col.alphaF() == 0)
		return ret;
	pdf_obj* color = pdf_new_array(context, 3/*RGB*/);
	pdf_array_push(color, pdf_new_real(context,col.redF()));
	pdf_array_push(color, pdf_new_real(context,col.greenF()));
	pdf_array_push(color, pdf_new_real(context,col.blueF()));
	pdf_dict_puts(ret,const_cast<char*>("C"),color);
	
	return ret;
}


PdfAnnotation* PdfAnnotation::fromPdfObj(pdf_obj *anno){
	QString subtype = pdf_to_name(pdf_dict_gets(anno,const_cast<char*>("Subtype")));
	if(subtype == "Ink")
		return new PdfInkAnnotation(anno);
	else if (subtype == "FreeText")
		return new PdfFreeTextAnnotation(anno);
	else if (subtype == "Text")
		return new PdfTextAnnotation(anno);
	else 
		return new PdfAnnotation(anno);
}

QRectF PdfAnnotation::rect(){
	return rect_prv;
}

void PdfAnnotation::setRect(QRectF r){
	rect_prv = r;
}

PdfInkAnnotation::PdfInkAnnotation() : PdfAnnotation("Ink") {}


PdfInkAnnotation::PdfInkAnnotation(pdf_obj *anno) : PdfAnnotation(anno)  {
	subtype = "Ink";
	
	pdf_obj* subpathsArr = pdf_dict_gets(anno,const_cast<char*>("InkList"));
	for(int i=0;i<pdf_array_len(subpathsArr);i++){
		pdf_obj* subpath = pdf_array_get(subpathsArr,i);
		if(pdf_array_len(subpath) >= 2)
			path.moveTo(pdf_to_real(pdf_array_get(subpath,0)),
				pdf_to_real(pdf_array_get(subpath,1)));
		for(int j=2;j+1<pdf_array_len(subpath);j+=2)
			path.lineTo(pdf_to_real(pdf_array_get(subpath,j)),
				pdf_to_real(pdf_array_get(subpath,j+1)));
	}
}


PdfInkAnnotation::PdfInkAnnotation(const PdfInkAnnotation &copy) : PdfAnnotation(copy) {
	path = copy.path;
}


PdfInkAnnotation& PdfInkAnnotation::operator=(PdfInkAnnotation &copy){
	PdfAnnotation::operator=(copy);
	path = copy.path;
	return *this;
}

//see iTexts: PdfAnnotation::createInk
pdf_obj* PdfInkAnnotation::toPdfObj(fz_context *context){
	pdf_obj* ret = PdfAnnotation::toPdfObj(context);
	
	QList<QPolygonF> subpaths = path.toSubpathPolygons();
	pdf_obj* subpathsArr = pdf_new_array(context, subpaths.length()/*int initialcap*/);
	for(int i=0;i<subpaths.length();i++){
		pdf_obj* subpath = pdf_new_array(context, subpaths[i].size()/*int initialcap*/);
		for(int j=0;j<subpaths[i].size();j++){
			pdf_array_push(subpath, pdf_new_real(context,subpaths[i][j].x()));
			pdf_array_push(subpath, pdf_new_real(context,subpaths[i][j].y()));
		}
		pdf_array_push(subpathsArr,subpath);
	}
	pdf_dict_puts(ret,const_cast<char*>("InkList"),subpathsArr);
	return ret;
}


QRectF PdfInkAnnotation::rect(){
	return path.boundingRect();
}

PdfFreeTextAnnotation::PdfFreeTextAnnotation() : PdfAnnotation("FreeText"), defApp("/Helvetica 16 Tf\n1 0 0 rg"), defStyle("font: Helvetica,sans-serif 16.0pt; text-align:left; color:#FF0000") {
	col = QColor("transparent");
	rect_prv = QRectF(10,10,30,30);
	contents = "Test";
}


PdfFreeTextAnnotation::PdfFreeTextAnnotation(pdf_obj *anno) : PdfAnnotation(anno)  {
	subtype = "FreeText";
	defApp = pdf_to_str_buf(pdf_dict_gets(anno,const_cast<char*>("DA")));
	defStyle = pdf_to_str_buf(pdf_dict_gets(anno,const_cast<char*>("DS")));
}


PdfFreeTextAnnotation::PdfFreeTextAnnotation(const PdfFreeTextAnnotation &copy) : PdfAnnotation(copy) {
	defApp = copy.defApp;
	defStyle = copy.defStyle;
}


PdfFreeTextAnnotation& PdfFreeTextAnnotation::operator=(PdfFreeTextAnnotation &copy){
	PdfAnnotation::operator=(copy);
	defApp = copy.defApp;
	defStyle = copy.defStyle;
	return *this;
}


pdf_obj* PdfFreeTextAnnotation::toPdfObj(fz_context *context){
	pdf_obj* ret = PdfAnnotation::toPdfObj(context);
	
	pdf_dict_puts(ret,const_cast<char*>("DA"),pdf_new_string(context,defApp.toLatin1().data(),defApp.length()));
	pdf_dict_puts(ret,const_cast<char*>("DS"),pdf_new_string(context,defStyle.toLatin1().data(),defStyle.length()));
	return ret;
}


PdfTextAnnotation::PdfTextAnnotation() : PdfAnnotation("Text"), open(true) {
}


PdfTextAnnotation::PdfTextAnnotation(pdf_obj *anno) : PdfAnnotation(anno)  {
	subtype = "Text";
	open = pdf_to_bool(pdf_dict_gets(anno,const_cast<char*>("Open")));
}


PdfTextAnnotation::PdfTextAnnotation(const PdfTextAnnotation &copy) : PdfAnnotation(copy) {
	open = copy.open;
}


PdfTextAnnotation& PdfTextAnnotation::operator=(PdfTextAnnotation &copy){
	PdfAnnotation::operator=(copy);
	open = copy.open;
	return *this;
}


pdf_obj* PdfTextAnnotation::toPdfObj(fz_context *context){
	pdf_obj* ret = PdfAnnotation::toPdfObj(context);
	pdf_dict_puts(ret,const_cast<char*>("Open"),pdf_new_bool(context,open));
	return ret;
}

PdfLink::PdfLink(QObject *parent) : QObject(parent) {
	type_prv = 0;
}

PdfLink::PdfLink(fz_link* fzl) : QObject(0) {
	position_prv = fz_rectToQRect(fzl->rect);
	fz_link_dest* fzd = &(fzl->dest);
	switch(fzd->kind){
		case FZ_LINK_GOTO:
		case FZ_LINK_GOTOR:
			type_prv = 1;//PDF Link
			destination_prv = fzd->ld.gotor.page+1;//starts with 0
			break;
		case FZ_LINK_URI:
			type_prv = 2;//URI
			destination_prv = QString(fzd->ld.uri.uri);
			break;
		default:
			type_prv = 0;
	}
}

PdfLink::PdfLink(const PdfLink& copy) : QObject(0) {
	position_prv = copy.position_prv;
	type_prv = copy.type_prv;
	destination_prv = copy.destination_prv;
}

	
PdfLink& PdfLink::operator=(const PdfLink& copy){
	position_prv = copy.position_prv;
	type_prv = copy.type_prv;
	destination_prv = copy.destination_prv;
	return *this;
}

QRect PdfLink::position(){
	return position_prv;
}

int PdfLink::type(){
	return type_prv;
}

QVariant PdfLink::destination(){
	return destination_prv;
}


QPdfImage::QPdfImage(ITEMC *parent) :
ITEMCI(parent) {
	SET_HAS_CONTENTS_FLAG
	setAcceptedMouseButtons(Qt::LeftButton | Qt::RightButton | Qt::MidButton);
	qRegisterMetaType<PdfLink>();
	qmlRegisterType<PdfLink>();
	curRegionIdx = -1;
	context = NULL;
	document = NULL;
	page = NULL;
	pixmap = NULL;
	text_page = NULL;
	pageobj = NULL;
	textMode_prv = false;
	textModeFontSize_prv = 12;
	pagenum_prv = 1;
	zoom_y_prv = zoom_prv = 100;
	totalpages_prv = 0;
	highlightPhrase_prv = "";
	caseSensitive_prv = false;
	drawMode_prv = false;
	hitOnPage = -1;
	clickTimer.setInterval(400);
	clickTimer.setSingleShot(true);
	connect(&clickTimer, SIGNAL(timeout()), this, SIGNAL(clickForward()));
	renderTimer.setInterval(1);
	renderTimer.setSingleShot(true);
	connect(&renderTimer, SIGNAL(timeout()), this, SLOT(render()));
	
	textDocument = QList<QString>();
	hitCount = QList<int>();
	
	QVector<QRgb> ct;
	ct << QColor(137,153,137,255).rgba ();
	ct << 0;
	noteImage = QImage(":/note_inv.png").scaled(24,24).convertToFormat(QImage::Format_Indexed8, ct);
	loadImage = QImage(":/load.png");
	
	image = new QImage(800, 800, QImage::Format_RGB32);
	QRgb value;
	value = qRgb(189, 149, 39); // 0xffbd9527
	image->fill(value);
	setSize(image->size());
}

QPdfImage::~QPdfImage(){
	if(image)
		delete image;
	if(pixmap)
		fz_drop_pixmap(context, pixmap);
	
	if(text_page)
		fz_free_text_page(context,text_page);
	
	if(document){
		fz_close_document(document);
		document = NULL;
	}
}


bool QPdfImage::loadFile(QString filename){
	if(!QFile::exists(filename))
		return false;
	if(image){
		delete image;
		image = NULL;
	}
	if(pixmap){
		fz_drop_pixmap(context, pixmap);
		pixmap = NULL;
	}
	if(text_page){
		fz_free_text_page(context,text_page);
		text_page = NULL;
	}
	textDocument.clear();
	regions.clear();
	curRegionIdx = -1;
	//NOTE:filename_prv has to be the old one:
	writeAnnotationsToPage();
	pageobj = NULL;
	
	if(document){
		fz_close_document(document);
		document = NULL;
	}
	
	//WARNING:filepath_prv set here!
	filepath_prv = filename;
	
	fileModified = false;
	
	if(!context)
		context = fz_new_context(NULL, NULL, FZ_STORE_UNLIMITED);
	
	
	pdf_document* xref = pdf_open_document(context, filename.toLocal8Bit().data());
	document = (fz_document*) xref;//fz_document is first member of pdf_document


	pdf_obj *info = pdf_dict_gets(xref->trailer,const_cast<char*>("Info"));
	
	pdf_obj *titleObj = pdf_dict_gets(info,const_cast<char*>("Title"));
	if(titleObj)
		title_prv = pdf_to_str_buf(titleObj);
	else 
		title_prv = "";
	emit titleChanged(title_prv);
	
	pdf_obj *authorObj = pdf_dict_gets(info,const_cast<char*>("Author"));
	if(authorObj)
		author_prv = pdf_to_str_buf(authorObj);
	else 
		author_prv = "";
	emit authorChanged(author_prv);

	
	totalpages_prv = fz_count_pages(document);
	emit totalPagesChanged(totalpages_prv);
	fz_outline *outl = fz_load_outline(document);
	outline_prv = outlineToVariantList(outl);
	emit outlineChanged();
	return true;
}

void QPdfImage::writeAnnotationsToPage(){
	if(!pageobj || !pageModifications)
		return;
	fileModified = true;
	pageModifications = 0;
	// before: old annotsArr: A1 A2  A3  ... An
	// now: annos:     A1 A2* A3- ... An An+1 An+2 ... Am
	//*modified
	//-deleted (only tag, still in list!)
	//n:length of annotsArr
	//m:length of annos (m>=n)
	
	pdf_obj *annotsArr = pdf_new_array(context, 1);
	
	for(int i=0;i < annos.length();i++)
		if(annos[i]->state != PdfAnnotation::DELETED)
			pdf_array_push(annotsArr,annos[i]->toPdfObj(context));

	pdf_dict_puts(pageobj, const_cast<char*>("Annots"), annotsArr);
}

void QPdfImage::readAnnotationsFromPage(){
	annos.clear();
	pdf_obj *annotsArr = pdf_dict_gets(pageobj, const_cast<char*>("Annots"));
	if(annotsArr  && pdf_is_array(annotsArr)){
		for(int i=0;i<pdf_array_len(annotsArr);i++){
			//if(pdf_to_name(pdf_dict_gets(pdf_array_get(annotsArr,i),"Subtype")) == QString("Ink"))
				annos << PdfAnnotation::fromPdfObj(pdf_array_get(annotsArr,i));
		}
	}
}


void QPdfImage::writeCurrentFile(){
	qDebug() << "asked for write to" + filepath_prv;
	writeAnnotationsToPage();
	bool writeSuccesful = true;
	if(fileModified){
		qDebug() << "will write to " + filepath_prv;
		fz_write_options opts;
		opts.do_garbage = 0;
		opts.do_expand = 0;
		opts.do_ascii = 0;
		opts.do_linear = 0;
			
		//set the locale to "C" (float format in PDFs uses ".")
		char* curLoc = setlocale(LC_NUMERIC,NULL);
		setlocale(LC_NUMERIC,"C");
		fz_try(context) {
			pdf_write_document((pdf_document*)document, (filepath_prv+"__flatboat_tmp").toLocal8Bit().data(),&opts);
		} fz_catch(context) {
			writeSuccesful = false;
		}
		setlocale(LC_NUMERIC,curLoc);
	}
	if(fileModified && writeSuccesful){
		QFile::remove(filepath_prv.toLocal8Bit().data());
		QFile::rename((filepath_prv+"__flatboat_tmp").toLocal8Bit().data(),filepath_prv.toLocal8Bit().data());
		fileModified = false;
	} else if(fileModified && !writeSuccesful){
		QFile::remove((filepath_prv+"__flatboat_tmp").toLocal8Bit().data());
	}
}


#include <QCoreApplication>
bool QPdfImage::gotoPage(int pagenum,bool doRender){
	if(!document || pagenum > totalpages_prv || pagenum < 1)
		return false;
	
	writeAnnotationsToPage();
	
	//delete the page reference
	if(page)
		fz_free_page(document, page);
	page = fz_load_page(document, pagenum - 1);
	pageobj = ((pdf_document*) document)->page_objs[pagenum-1];
	
	//render page to pixmap
	fz_matrix transform = fz_scale(zoom_prv/100.0,zoom_y_prv/100.0);
	transform = fz_concat(transform, fz_rotate(0));
	fz_rect rect = fz_bound_page(document, page);//transformations are calculated using float
	originalWidth_prv = rect.x1 - rect.x0;
	originalHeight_prv = rect.y1 - rect.y0;
	rect = fz_transform_rect(transform, rect);
	//fz_bbox bbox = fz_round_rect(rect); /*bounding box is int,
	//fz_round_rect may be smaller than fz_bbox_covering_rect */
	
	
	//extract text
	if(text_page){
		fz_free_text_page(context,text_page);
		text_page = NULL;
	}
	fz_text_sheet *text_sheet = fz_new_text_sheet(context);//stores styles
	text_page = fz_new_text_page(context, rect);//stores "plain" text
	fz_device *text_device = fz_new_text_device(context, text_sheet, text_page);
	fz_run_page(document, page, text_device, transform, NULL);
	fz_free_device(text_device);
	
	highlight.clear();
	highlight += searchInPage(text_page,highlightPhrase_prv,caseSensitive_prv);
	regions.clear();
	regions += textAreas(text_page,zoom_prv);
	if(regions.length() > 0){
		if(curRegionIdx == -1 || curRegionIdx >= regions.length())
			curRegionIdx = 0;
	} else
		curRegionIdx = -1;
	
	fz_link* fz_links = fz_load_links(document, page);
	links_prv.clear();
	while(fz_links){
		fz_links->rect = fz_transform_rect(transform, fz_links->rect);
		links_prv.append(PdfLink(fz_links));
		fz_links = fz_links->next;
	}
	
	if(doRender){
		isLoading = true;
		update();
		QCoreApplication::processEvents();
		renderTimer.start();
	}
// 	QFuture<void> fut;
// 	if(doRender)  fut = QtConcurrent::run(this,&QPdfImage::render);
// 	fut.waitForFinished();

	if(pagenum_prv != pagenum){
		pagenum_prv = pagenum;
		emit pagenumChanged(pagenum_prv);
	}
	
	emit originalWidthChanged();
	emit originalHeightChanged();
	return true;
}

void QPdfImage::render(){
	fz_matrix transform = fz_scale(zoom_prv/100.0,zoom_y_prv/100.0);
	transform = fz_concat(transform, fz_rotate(0));
	fz_bbox bbox = fz_round_rect(fz_transform_rect(transform,fz_bound_page(document, page)));

	//save the pointer to the old pixmap
	fz_pixmap* oldpm = pixmap;
	
	pixmap = fz_new_pixmap_with_bbox(context, fz_device_bgr, bbox);//bgr matches Qt's rgb!
	fz_clear_pixmap_with_value(context, pixmap, 255);//draw on white background
	fz_device *device = fz_new_draw_device(context, pixmap);
	fz_run_page(document, page, device, transform, NULL);
	fz_free_device(device);
	
	//prepare the raw image
	unsigned char *samples = fz_pixmap_samples(context, pixmap);
	int width = fz_pixmap_width(context, pixmap);
	int height = fz_pixmap_height(context, pixmap);
	
	if(image)
		delete image;
	image = new QImage(samples, width, height, QImage::Format_ARGB32);
	setSize(image->size());

	readAnnotationsFromPage();
	pageModifications = 0;
	
	isLoading = false;
	emit renderingFinished();
	update();
	
	//delete old pixmap
	fz_drop_pixmap(context, oldpm);
}

void QPdfImage::askForRendering(){
	isLoading = true;
	update();
	QCoreApplication::processEvents();
	renderTimer.start();
}

void QPdfImage::paint(QPainter *painter ADD_PAINT_PAR) {
	if(isLoading){
		painter->setPen(Qt::black);
		painter->setBrush(Qt::black);
		QFont fnt = painter->font();
		fnt.setPixelSize(width()/20);
		painter->setFont(fnt);
		painter->drawImage(QPoint(0,0),loadImage.scaled(width(),height()));
		painter->drawText(QRect(0.45*width(),0.4*height(),0.3*width(),0.3*height()),
			Qt::AlignLeft|Qt::TextDontClip|Qt::TextSingleLine,
			"loading ...");
		return;
	}
		
	if(textMode_prv){
		painter->setPen(Qt::black);
		painter->setBrush(Qt::black);
		if(textDocument.isEmpty())
			textDocument += document2stringList();
		QLabel* ql = new QLabel(adjustPageString(textDocument[pagenum_prv-1],-1/*auto*/));
		QFont fnt = ql->font();
		fnt.setPointSize(textModeFontSize_prv);
		ql->setFont(fnt);
		setWidth(parentItem()->width());
		setHeight(ql->heightForWidth(width()));
		ql->resize(width(),height());
		ql->setMargin(20);
		//painter->translate(5,5);
		ql->setStyleSheet("color:rgba(0, 0, 0, 255); background-color: rgba(0, 0, 0, 0);");
		ql->setWordWrap(true);
// 		painter->drawText(QRect(0,0,0.8*width(),3*height()),
// 			Qt::AlignLeft|Qt::TextDontClip|Qt::TextSingleLine|Qt::TextWordWrap,
// 			textDocument[pagenum_prv-1]);
		ql->setFrameStyle(QFrame::Panel | QFrame::Sunken);
		ql->render(painter);
		return;
	}
	if(grainyMode_prv){
// 		painter->setPen(QColor ( 0, 0, 0, 255));
// 		painter->setBrush(QBrush(QColor ("transparent")));
// 		for (fz_text_block *block = text_page->blocks; block < text_page->blocks + text_page->len; block++){
// 			painter->drawRect(fz_rectToQRect(block->bbox));
// 		}
		for(int i=0;i < regions.length();i++){
			painter->drawRect(regions[i]);
		}
		return;
	}
	if(image)
		painter->drawImage (QPoint(0,0),*image);
	painter->setPen(QColor("transparent"));
	painter->setBrush(QBrush(QColor ( 230, 230, 30, 120)));
	for(int i=0;i < highlight.length();i++)
		painter->drawRect(highlight[i]);
	painter->setBrush(QBrush(QColor ( 255, 0, 0, 80)));
	if(hitOnPage > -1 && hitOnPage < highlight.length() && lastSearchPhrase == highlightPhrase_prv)
		painter->drawRect(highlight[hitOnPage]);
		
// 	draw rectangles around links
	if(linkHints_prv){
		painter->setPen(QColor ( 117, 117, 117, 30));
		painter->setBrush(QBrush(QColor ("transparent")));
		for(int i=0;i < links_prv.length();i++){
			painter->drawRect(links_prv[i].position());
		}
	}
	
	
	painter->setBrush(QBrush(QColor ("transparent")));
	for(int i=0;i<annos.length();i++){
		if(annos[i]->state == PdfAnnotation::DELETED)
			continue;
		painter->setPen(annos[i]->col);
		if(annos[i]->subtype == "Ink"){
			painter->save();
			painter->translate(0,height());
			painter->scale(zoom_prv/100.0,-zoom_y_prv/100.0);
			painter->drawPath(((PdfInkAnnotation*) annos[i])->path);
			painter->restore();
		} else if(annos[i]->subtype == "FreeText"){
			PdfFreeTextAnnotation* ftAnno  = (PdfFreeTextAnnotation*) annos[i];
			QRectF r = ftAnno->rect();
			painter->drawText(QRectF(
					r.x()*zoom_prv/100.0,
					height() - (r.y()+r.height())*zoom_y_prv/100.0,
					r.width()*zoom_prv/100.0,
					r.height()*zoom_y_prv/100.0),
				ftAnno->contents);
			int d = ftAnno->defApp.lastIndexOf("Tf");
			QFont fnt = painter->font();
			fnt.setPointSize(ftAnno->defApp.mid(d-3,2).toInt() >= 0 ? ftAnno->defApp.mid(d-3,2).toInt() : 12);
			painter->setFont(fnt);
		} else if(annos[i]->subtype == "Text"){
			PdfTextAnnotation* noteAnno  = (PdfTextAnnotation*) annos[i];
			QRectF r = noteAnno->rect();
			noteImage.setColor(0,noteAnno->col.rgba());
			painter->drawImage(QPointF(
					r.x()*zoom_prv/100.0,
					height() - (r.y()+r.height())*zoom_y_prv/100.0),noteImage);
			if(!noteAnno->open)
				continue;
			painter->drawText(QRectF(
					r.x()*zoom_prv/100.0+24,
					height() - (r.y()+r.height())*zoom_y_prv/100.0+24,
					100*zoom_prv/100.0,
					100*zoom_y_prv/100.0),
					Qt::TextWordWrap | Qt::TextDontClip,
				noteAnno->contents);
		}
	}

	
// // 	debug the column finder code
// 	painter->setPen(QColor ( 255, 0, 0, 255));
// 	painter->setBrush(QBrush(QColor ("transparent")));
// 	for(int i=0;i < regions.length();i++){
// 		painter->drawRect(regions[i]);
// 		painter->drawText(regions[i].regions(),QString::number(i));
// 	}
}

void QPdfImage::mousePressEvent(MOUSE_EVENTC *event){
	emit allPressForward();
	if(drawMode_prv){
		
		//earlier an annotation was created when the draw mode was entered
		//now this is done every time the user clicks while being in draw mode
		PdfInkAnnotation* inkAnno = new PdfInkAnnotation();
		pageModifications++;//we'll add a path to the page
		inkAnno->col = drawColor_prv;
		inkAnno->date = QDateTime::currentDateTime();
		annos << inkAnno;
// 		PdfInkAnnotation* inkAnno = (PdfInkAnnotation*)  annos.last();
		
		inkAnno->path.moveTo(event->pos().x()*100/zoom_prv,originalHeight_prv - event->pos().y()*100/zoom_y_prv);
		update();
		return;
	}
	if(eraseMode_prv){
		QRect clickRect(
			event->pos().x()*100/zoom_prv,
			originalHeight_prv - event->pos().y()*100/zoom_y_prv,
			32*100/zoom_prv,
			32*100/zoom_y_prv);
		for(int i=0;i<annos.length();i++)
			if(annos[i]->rect().intersects(clickRect)){
				annos[i]->state = PdfInkAnnotation::DELETED;
				pageModifications++;
			}
		update();
		return;
	}
	if(noteMode_prv){
		annos << new PdfTextAnnotation();
		pageModifications++;//we'll add a text annotation to the page
		annos.last()->date = QDateTime::currentDateTime();
		annos.last()->col = drawColor_prv;
		annos.last()->setRect(QRectF(event->pos().x()*100/zoom_prv,
			originalHeight_prv - event->pos().y()*100/zoom_y_prv - 24,
			24,
			24));
		emit newNote();
		update();
		return;
	}
	QPoint clickP = TOPOINT(event->pos());
	for(int i=0;i<links_prv.length();i++){
		if(links_prv[i].position().contains(clickP)){
			emit linkSelected(links_prv[i].destination());
// 			(qDebug() << "hit") << i << links_prv[i].destination().toInt(); 
			return;
		}
	}
	for(int i=0;i<annos.length();i++){
		QRectF r = annos[i]->rect();

		if(annos[i]->subtype == "Text" 
			&& QRect(r.x()*zoom_prv/100.0,
			height() - (r.y()+r.height())*zoom_y_prv/100.0,24,24).contains(clickP)){
			
			((PdfTextAnnotation*) annos[i])->open = !((PdfTextAnnotation*) annos[i])->open;
			pageModifications++;
			update();
			return;
		}
	}
	
	
	clickTimer.start();
}

void QPdfImage::mouseReleaseEvent(MOUSE_EVENTC*){
	if(drawMode_prv || eraseMode_prv || noteMode_prv){
		return;
	}
	clickTimer.stop();
}

void QPdfImage::mouseDoubleClickEvent(MOUSE_EVENTC*){
	if(drawMode_prv || eraseMode_prv || noteMode_prv){
		return;
	}
	clickTimer.stop();
	emit clickForward();
}

void QPdfImage::mouseMoveEvent(MOUSE_EVENTC *event){
	if(drawMode_prv){
		((PdfInkAnnotation*) annos.last())->path.lineTo(event->pos().x()*100/zoom_prv,originalHeight_prv -event->pos().y()*100/zoom_y_prv);
		update();
		return;
	}
}

void QPdfImage::stopClickTimer(){
	clickTimer.stop();
}

QStringList QPdfImage::document2stringList(){
	QStringList textDocument;
	fz_page *page;
	fz_text_page *text_page;
	fz_text_sheet *text_sheet;
	fz_device *text_device;
	if(!document)
		return textDocument;
	for(int i=0;i<totalpages_prv;i++){
		page = fz_load_page(document,i);
		text_sheet = fz_new_text_sheet(context);//stores styles
		text_page = fz_new_text_page(context, fz_infinite_rect);//stores "plain" text
		text_device = fz_new_text_device(context, text_sheet, text_page);
		fz_run_page(document, page, text_device, fz_identity, NULL);
		textDocument << page2string(text_page);
		fz_free_device(text_device);
		fz_free_text_page(context,text_page);
		fz_free_text_sheet(context,text_sheet);
		fz_free_page(document,page);
	}
	return textDocument;
}

bool QPdfImage::searchNext(QString str){
	if(textDocument.isEmpty())
		textDocument += document2stringList(); 
	if(str != lastSearchPhrase || caseSensitive_prv != lastSearchCaseSensitive){
		hitCount.clear();
		hitOnPage = -1;
		for(int i=0;i<totalpages_prv;i++)
			hitCount.append(textDocument[i].count(str, (Qt::CaseSensitivity) caseSensitive_prv));
	}
	lastSearchCaseSensitive = caseSensitive_prv;
	lastSearchPhrase = str;
	if(hitOnPage < hitCount[pagenum_prv-1]-1){
		hitOnPage++;	
		update();
	} else {
		int nextPage = pagenum_prv == totalpages_prv ? 1 : pagenum_prv+1;
		while(nextPage != pagenum_prv && hitCount[nextPage-1] == 0)
			nextPage = nextPage == totalpages_prv ? 1 : nextPage+1;
		if(hitCount[nextPage-1] != 0)
			hitOnPage = 0;
		else
			hitOnPage = -1;
		if(nextPage != pagenum_prv)
			gotoPage(nextPage);
		else
			update();
	}
	//set the corresponding region if phrase is found
	if(hitOnPage >= 0){
		QRect hitRect = highlight[hitOnPage];
		int i = 0;
		for(;i < regions.length() && regions[i].intersects(hitRect);i++) {}
		if(i < regions.length())
			curRegionIdx = i;
		else
			curRegionIdx = -1;
	}
	emit hitRectChanged();
	return (bool) (hitOnPage+1);
}

bool QPdfImage::searchPrev(QString str){
	if(textDocument.isEmpty())
		textDocument += document2stringList(); 
	if(str != lastSearchPhrase || caseSensitive_prv != lastSearchCaseSensitive){
		hitCount.clear();
		hitOnPage = -1;
		for(int i=0;i<totalpages_prv;i++)
			hitCount.append(textDocument[i].count(str, (Qt::CaseSensitivity) caseSensitive_prv));
	}
	lastSearchCaseSensitive = caseSensitive_prv;
	lastSearchPhrase = str;
	if(hitOnPage > 0){
		hitOnPage--;	
		update();
	} else {
		int prevPage = pagenum_prv == 1 ? totalpages_prv : pagenum_prv-1;
		while(prevPage != pagenum_prv && hitCount[prevPage-1] == 0)
			prevPage = prevPage == 1 ? totalpages_prv : prevPage-1;
		if(hitCount[prevPage-1] != 0)
			hitOnPage = hitCount[prevPage-1]-1;//last hit
			else
				hitOnPage = -1;
			if(prevPage != pagenum_prv)
				gotoPage(prevPage);
			else
				update();
	}
	//set the corresponding region if phrase is found
	if(hitOnPage >= 0){
		QRect hitRect = highlight[hitOnPage];
		int i = 0;
		for(;i < regions.length() && regions[i].intersects(hitRect);i++) {}
		if(i < regions.length())
			curRegionIdx = i;
		else
			curRegionIdx = -1;
	}
	emit hitRectChanged();
	return (bool) (hitOnPage+1);
}

QRect QPdfImage::prevRegion(bool render){
	if(curRegionIdx > 0){
		emit regionIdxChanged(--curRegionIdx);
		return regions[curRegionIdx];
	}
	if(pagenum_prv > 1)
		gotoPage(pagenum_prv-1,render);//sets regions
	else
		gotoPage(totalpages_prv,render);//sets regions
	curRegionIdx = regions.length()-1;//may be -1, i.e. invalid
	
	emit regionIdxChanged(curRegionIdx);
	return curRegion();
}

QRect QPdfImage::curRegion(){
	if(curRegionIdx >= 0)
		return regions[curRegionIdx];
	else //if no text is present, show the entire page
		return QRect(0,0,width(),height());
}

QRect QPdfImage::nextRegion(bool render){
	if(curRegionIdx < regions.length()-1){
		emit regionIdxChanged(++curRegionIdx);
		return regions[curRegionIdx];
	}
	if(pagenum_prv < totalpages_prv)
		gotoPage(pagenum_prv+1,render);//sets regions
	else
		gotoPage(1,render);//sets regions
	if(regions.length() > 0)
		curRegionIdx = 0;
	else
		curRegionIdx = -1;
	emit regionIdxChanged(curRegionIdx);
	return curRegion();
}

int QPdfImage::regionIdx(){
	return curRegionIdx;
}

QRect QPdfImage::setRegionIdx(int idx){
	if(idx < regions.length() && idx >= -1)
		curRegionIdx = idx;
	else if(regions.length() > 0)
		curRegionIdx = 0;
	else
		curRegionIdx = -1;
	emit regionIdxChanged(curRegionIdx);
	return curRegion();
}



int QPdfImage::totalPages(){
	return totalpages_prv;
}

QString QPdfImage::filepath(){
	return filepath_prv;
}

bool QPdfImage::textMode(){
	return textMode_prv;
}
void QPdfImage::setTextMode(bool tm){
	if(textMode_prv == tm)
		return;
	textMode_prv = tm;
	update();
	emit textModeChanged(tm);
}

bool QPdfImage::grainyMode(){
	return grainyMode_prv;
}
void QPdfImage::setGrainyMode(bool gm){
	if(grainyMode_prv == gm)
		return;
	grainyMode_prv = gm;
	update();
	emit grainyModeChanged(gm);
}

int QPdfImage::textModeFontSize(){
	return textModeFontSize_prv;
}
void QPdfImage::setTextModeFontSize(int fs){
	if(textModeFontSize_prv == fs || fs <= 0)
		return;
	textModeFontSize_prv = fs;
	update();
	emit textModeFontSizeChanged();
}

int QPdfImage::pagenum(){
	return pagenum_prv;
}

int QPdfImage::zoom(){
	return zoom_prv;
}

bool QPdfImage::setZoom(int percent,bool render){
	percent = percent > 300 ? 300 : percent;
	if(zoom_prv == percent)
		return true;
	zoom_y_prv = zoom_prv = percent;
	int ret = gotoPage(pagenum_prv,render);
	emit zoomChanged(zoom_prv);
	return ret;
}

bool QPdfImage::stretch(int w,int h){
	zoom_prv *= w/width();
	zoom_y_prv *= h/height();
	int ret = gotoPage(pagenum_prv);
	emit zoomChanged(zoom_prv);
	emit stretchedChanged();
	return ret;
}

bool QPdfImage::stretched(){
	return zoom_prv == zoom_y_prv;
}

int QPdfImage::originalWidth(){
	return originalWidth_prv;
}

int QPdfImage::originalHeight(){
	return originalHeight_prv;
}

QString QPdfImage::highlightPhrase(){
	return highlightPhrase_prv;
}
void QPdfImage::setHighlightPhrase(QString phrase){
	highlightPhrase_prv = phrase;
	highlight.clear();
	if(!text_page)
		return;
	highlight += searchInPage(text_page,highlightPhrase_prv,caseSensitive_prv);
	update();
}


QList<PdfLink> QPdfImage::links(){
	return links_prv;
}

bool QPdfImage::linkHints(){
	return linkHints_prv;
}
void QPdfImage::setLinkHints(bool lh){
	if(linkHints_prv == lh)
		return;
	linkHints_prv = lh;
	update();
	emit linkHintsChanged(lh);
}

bool QPdfImage::caseSensitive(){
	return caseSensitive_prv;
}
void QPdfImage::setCaseSensitive(bool cs){
	caseSensitive_prv = cs;
	setHighlightPhrase(highlightPhrase_prv);//update highlighting
}

QRect QPdfImage::hitRect(){
	if(hitOnPage >=0)
		return highlight[hitOnPage];
	else
		return QRect(0,0,0,0);
}

bool QPdfImage::drawMode(){
	return drawMode_prv;
}


void QPdfImage::setDrawMode(bool dm){
	if(dm == drawMode_prv)
		return;
	if(dm){
		setEraseMode(false);
		setNoteMode(false);
		
		//earlier an annotation was created when the draw mode was entered
		//now this is done every time the user clicks while being in draw mode
// 		annos << new PdfInkAnnotation();
// 		pageModifications++;//we'll add a path to the page (s.b.)
// 		annos.last()->col = drawColor_prv;
// 		annos.last()->date = QDateTime::currentDateTime();
	}
	drawMode_prv = dm;
	emit drawModeChanged(dm);
	
	//no longer needed, see above
// 	if(!dm){
// 		if(!((PdfInkAnnotation*) annos.last())->path.elementCount()){
// 			annos.removeLast();//no, we don't: it's empty
// 			pageModifications--;//undo false increasement (s.a.)
// 		}
// 	}
}

bool QPdfImage::eraseMode(){
	return eraseMode_prv;
}

void QPdfImage::setEraseMode(bool em){
	if(em == eraseMode_prv)
		return;
	eraseMode_prv = em;
	emit eraseModeChanged(em);
	if(em){
		setDrawMode(false);
		setNoteMode(false);
	}
}

bool QPdfImage::noteMode(){
	return noteMode_prv;
}

void QPdfImage::setNoteMode(bool ftm){
	if(ftm == noteMode_prv)
		return;
	noteMode_prv = ftm;
	emit noteModeChanged(ftm);
	if(ftm){
		setDrawMode(false);
		setEraseMode(false);
	}
}

bool QPdfImage::modified(){
	writeAnnotationsToPage();
	return fileModified;
}

QColor QPdfImage::drawColor(){
	return drawColor_prv;
}

void QPdfImage::setDrawColor(QColor cl){
	if(drawColor_prv == cl)
		return;
	drawColor_prv = cl;
	emit drawColorChanged();
}

void QPdfImage::updateAnnoText(QString atext){
	if(noteMode_prv){
		if(annos.isEmpty() || (annos.last()->subtype != "Text"))
			return;
		((PdfTextAnnotation*) annos.last())->contents = atext;
		update();
		return;
	}
}

QString QPdfImage::author(){
	return author_prv;
}

QString QPdfImage::title(){
	return title_prv;
}

QVariantList QPdfImage::outline(){
	return outline_prv;
}
