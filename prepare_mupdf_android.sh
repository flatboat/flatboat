#!/bin/bash
cd `dirname $0`
./prepare_mupdf_linux.sh
cd ../mupdf-git
ed android/jni/Core.mk <<STOP
59a
	\$(MY_ROOT)/fitz/res_bitmap.c .
.
w
q
STOP

cd android 
ndk-build
ant debug
