/* This file is part of "modern computer flatboat", a pdf viewer.
 * Copyright (C) 2012,2013 Frank Fuhlbrück 
 * License: GPLv3 (or any later version, at your option)
 * See the file "LICENSE".
 */

#include "qappintentactivity.h"
#include "sethist.h"
#include <QFileOpenEvent>
#include <QUrl>
#include <QStringList>

QStringList QAppIntentActivity::earlyOpen = QStringList(); 

QAppIntentActivity::QAppIntentActivity(int& argc, char** argv) : QApplication(argc, argv){
	connected = false;
}

#if QT_VERSION < 0x050000
	void QAppIntentActivity::connectNotify(const char* signal){
#else
	void QAppIntentActivity::connectNotify(const QMetaMethod& signalMethod){
	QByteArray signal = signalMethod.name();
#endif

	connected = true;
	if (QLatin1String(signal) == SIGNAL(fileOpen(QString)) && earlyOpen.length() > 0){
		for(int i =0;i<earlyOpen.length();i++)	{
			emit fileOpen(QUrl(earlyOpen[i]).toLocalFile());
		}
		earlyOpen.clear();
	}
}



bool QAppIntentActivity::event(QEvent *event){
	switch (event->type()) {
		case QEvent::FileOpen:
			emit fileOpen(((QFileOpenEvent*) event)->file());
			return true;
		default:
			return QApplication::event(event);
	}
}


void QAppIntentActivity::onViewIntent(QString uri){
	earlyOpen << uri;
	if(connected)
		emit fileOpen(QUrl(uri).toLocalFile());
// 	else
// 		earlyOpen << uri;
}

#ifdef Q_OS_ANDROID
#include <jni.h>
#define JNOTOKRETURN(WHAT) if(!WHAT || jenv->ExceptionCheck()){SetHist::log("WHAT went wrong");jenv->ExceptionClear();return JNI_VERSION_1_4;}
void passViewIntent(JNIEnv *jenv, jobject,jstring uri){
	const char* curi = jenv->GetStringUTFChars(uri, 0);    
	QString quri = curi;
	
	QAppIntentActivity* app = (QAppIntentActivity*) QAppIntentActivity::instance();
	if(app != NULL)
		app->onViewIntent(quri);
	else
		QAppIntentActivity::earlyOpen << quri;

	jenv->ReleaseStringUTFChars(uri, curi);
}
#endif