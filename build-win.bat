set QTPATH=c:\Qt\4.8.3\
set QTBIN=%QTPATH%bin\
set oldp=%cd%
set type=release
md ..\flatboat-build-win
cd ..\flatboat-build-win
qmake ..\flatboat\flatboat.pro
mingw32-make %type%
md %type%\flatboat
copy %type%\flatboat.exe %type%\flatboat
copy %QTBIN%QtCore4.dll %type%\flatboat & copy %QTBIN%QtGui4.dll %type%\flatboat & copy %QTBIN%QtDeclarative4.dll %type%\flatboat & copy %QTBIN%QtNetwork4.dll %type%\flatboat & copy %QTBIN%QtScript4.dll %type%\flatboat & copy %QTBIN%QtSql4.dll %type%\flatboat & copy %QTBIN%QtXmlPatterns4.dll %type%\flatboat & copy C:\MinGW\bin\mingwm10.dll %type%\flatboat & copy C:\MinGW\bin\libgcc_s_dw2-1.dll %type%\flatboat & copy "C:\MinGW\bin\libstdc++-6.dll" %type%\flatboat
cd %oldp%