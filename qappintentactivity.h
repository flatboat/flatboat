/* This file is part of "modern computer flatboat", a pdf viewer.
 * Copyright (C) 2012,2013 Frank Fuhlbrück 
 * License: GPLv3 (or any later version, at your option)
 * See the file "LICENSE".
 */
#ifndef QAPP_INTENT_ACTIVITY_H
#define QAPP_INTENT_ACTIVITY_H

#include <QVariant>
#if QT_VERSION < 0x050000
#include <QtGui/QApplication>
#else
#include <QMetaMethod>
#include <QtWidgets/QApplication>
#endif

class QAppIntentActivity : public QApplication {
	Q_OBJECT
	
public:
	QAppIntentActivity(int& argc, char** argv);
	void onViewIntent(QString uri);
	
	static QStringList earlyOpen;
	
protected:
	bool event(QEvent *event);
#if QT_VERSION < 0x050000
	void connectNotify(const char* signal);
#else
	void connectNotify(const QMetaMethod& signalMethod);
#endif
	
signals:
	void fileOpen(const QString &fileName);
	
private:
	bool connected;
};

#ifdef Q_OS_ANDROID
#include <jni.h>
void passViewIntent(JNIEnv *jenv, jobject,jstring uri);
#endif

#endif // QAPP_INTENT_ACTIVITY_H
