cd `dirname $0`
mkdir -p ../flatboat-build-maemo
cd ../flatboat-build-maemo
mad qmake CONFIG+=maemo-local-debug ../flatboat/flatboat.pro
mad make
if [ -n "$1" ]; then
	scp ./flatboat ${1}:/opt/maemo/usr/bin/flatboat
	if [ -n "$2" ]; then
		scp ../flatboat/flatboat128.png /opt/maemo/usr/share/icons/hicolor/128x128/apps/flatboat.png
		ssh $1 "ln -s /opt/maemo/usr/share/icons/hicolor/128x128/apps/flatboat.png /usr/share/icons/hicolor/128x128/apps/flatboat.png"
		scp ../flatboat/flatboat.desktop $1:/usr/share/applications/hildon/flatboat.desktop
		ssh $1 "ln -s /opt/maemo/usr/bin/flatboat /usr/bin/flatboat"
	fi
fi
