# This file is part of "modern computer flatboat", a pdf viewer.
# Copyright (C) 2012 Frank Fuhlbrück 
# License: GPLv3 (or any later version, at your option)
# See the file "LICENSE".

# qml files
qml_folder.source = qml/flatboat
qml_folder.target = qml
DEPLOYMENTFOLDERS = qml_folder

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =

# Speed up launching on MeeGo/Harmattan when using applauncherd daemon
# CONFIG += qdeclarative-boostable

# Add dependency to Symbian components
# CONFIG += qt-components

QT += widgets quick 

eink {
	DEFINES += EINK_DISPLAY=1
}

SOURCES += main.cpp \
    qpdfimage.cpp \
    sethist.cpp \
    qappintentactivity.cpp
    
#mupdf inludes and libraries (modify ) 
android {
LIBS += -L$$PWD/../mupdf-git/android/obj/local/armeabi/ -lmupdfcore -lmupdfthirdparty  /opt/NecessitasQtSDK/android-ndk/toolchains/arm-linux-androideabi-4.4.3/prebuilt/linux-x86/lib/gcc/arm-linux-androideabi/4.4.3/libgcc.a 
INCLUDEPATH += $$PWD/../mupdf-git/fitz/ $$PWD/../mupdf-git/pdf/
} else:maemo-local-debug {
LIBS += $$PWD/../mupdf-git/build/debug-maemo/libfitz.a  $$PWD/../mupdf-git/build/debug-maemo/libjpeg.a  $$PWD/../mupdf-git/build/debug-maemo/libjbig2dec.a  -lopenjpeg -lfreetype -lz 
INCLUDEPATH += $$PWD/../mupdf-git/fitz/ $$PWD/../mupdf-git/pdf/ 
} else:maemo5 {
LIBS += -lfitz -ljpeg -ljbig2dec -lopenjpeg -lfreetype -lz 
} else:harmattan {
LIBS += $$PWD/../mupdf-git/build/debug-harmattan/libfitz.a  $$PWD/../mupdf-git/build/debug-harmattan/libjpeg.a  $$PWD/../mupdf-git/build/debug-harmattan/libjbig2dec.a  $$PWD/../mupdf-git/build/debug-harmattan/libopenjpeg.a $$PWD/../mupdf-git/build/debug-harmattan/libfreetype.a -lz 
INCLUDEPATH += $$PWD/../mupdf-git/fitz/ $$PWD/../mupdf-git/pdf/ 
} else:macx {
LIBS += -L$$PWD/../mupdf-git/build/debug-mac/ -lfitz $$PWD/../mupdf-git/build/debug-mac/libjpeg.a $$PWD/../mupdf-git/build/debug-mac/libfreetype.a -ljbig2dec -lz -lopenjpeg
INCLUDEPATH += $$PWD/../mupdf-git/fitz/ $$PWD/../mupdf-git/pdf/ 
} else:unix-local-debug {
LIBS += -L$$PWD/../mupdf-git/build/debug-linux/ -lfitz $$PWD/../mupdf-git/build/debug-linux/libjpeg.a -lfreetype -ljbig2dec -lz -lopenjpeg
INCLUDEPATH += $$PWD/../mupdf-git/fitz/ $$PWD/../mupdf-git/pdf/ 
} else:unix {
LIBS += -lfitz -ljpeg -ljbig2dec -lopenjpeg -lfreetype -lz 
} else:win32{
LIBS += -L$$PWD/../mupdf-git/build/debug/ -lfitz $$PWD/../mupdf-git/build/debug/libjpeg.a -lfreetype -ljbig2dec -lz -lopenjpeg
INCLUDEPATH += $$PWD/../mupdf-git/fitz/ $$PWD/../mupdf-git/pdf/
RC_FILE = winicon.rc
}

# Please do not modify the following two lines. Required for deployment.
include(qmlapplicationviewer/qmlapplicationviewer.pri)
qtcAddDeployment()

OTHER_FILES += \
    android/src/org/kde/necessitas/ministro/IMinistroCallback.aidl \
    android/src/org/kde/necessitas/ministro/IMinistro.aidl \
    android/src/org/kde/necessitas/origo/QtApplication.java \
    android/src/org/kde/necessitas/origo/QtActivity.java \
    android/AndroidManifest.xml \
    android/version.xml \
    android/res/values-zh-rCN/strings.xml \
    android/res/layout/splash.xml \
    android/res/values-ja/strings.xml \
    android/res/values-fr/strings.xml \
    android/res/values-pt-rBR/strings.xml \
    android/res/values-nl/strings.xml \
    android/res/values-it/strings.xml \
    android/res/values-id/strings.xml \
    android/res/values-zh-rTW/strings.xml \
    android/res/values-ms/strings.xml \
    android/res/values-pl/strings.xml \
    android/res/drawable-ldpi/icon.png \
    android/res/values-nb/strings.xml \
    android/res/values-fa/strings.xml \
    android/res/values-ro/strings.xml \
    android/res/values-es/strings.xml \
    android/res/values-el/strings.xml \
    android/res/drawable-hdpi/icon.png \
    android/res/drawable-mdpi/icon.png \
    android/res/values-rs/strings.xml \
    android/res/values-ru/strings.xml \
    android/res/values-et/strings.xml \
    android/res/drawable/logo.png \
    android/res/drawable/icon.png \
    android/res/values/strings.xml \
    android/res/values/libs.xml \
    android/res/values-de/strings.xml \
    android/src/org/kde/necessitas/ministro/IMinistroCallback.aidl \
    android/src/org/kde/necessitas/ministro/IMinistro.aidl \
    android/src/org/kde/necessitas/origo/QtApplication.java \
    android/src/org/kde/necessitas/origo/QtActivity.java \
    android/version.xml \
    android/res/values-zh-rCN/strings.xml \
    android/res/layout/splash.xml \
    android/res/values-ja/strings.xml \
    android/res/values-fr/strings.xml \
    android/res/values-pt-rBR/strings.xml \
    android/res/values-nl/strings.xml \
    android/res/values-it/strings.xml \
    android/res/values-id/strings.xml \
    android/res/values-zh-rTW/strings.xml \
    android/res/values-ms/strings.xml \
    android/res/values-pl/strings.xml \
    android/res/values-nb/strings.xml \
    android/res/values-fa/strings.xml \
    android/res/values-ro/strings.xml \
    android/res/values-es/strings.xml \
    android/res/values-el/strings.xml \
    android/res/values-rs/strings.xml \
    android/res/values-ru/strings.xml \
    android/res/values-et/strings.xml \
    android/res/values/strings.xml \
    android/res/values-de/strings.xml \
    android/AndroidManifest.xml \
    android/res/drawable-ldpi/icon.png \
    android/res/drawable-hdpi/icon.png \
    android/res/drawable-mdpi/icon.png \
    android/res/drawable/logo.png \
    android/res/drawable/icon.png \
    android/res/values/libs.xml

RESOURCES += \
    resources.qrc

HEADERS += \
    qpdfimage.h \
    sethist.h \
    qappintentactivity.h

macx:ICON=flatboat.icns
    
maemo5 {
INSTALLS    += target
target.path  = /opt/maemo/usr/bin/
} else:harmattan {
INSTALLS    += target
target.path  = /opt/flatboat/bin/

INSTALLS    += icon128
icon128.path  = /usr/share/icons/hicolor/128x128/apps/
icon128.files  = flatboat128.png
} else:unix {
INSTALLS    += target
target.path  = /usr/bin/
}
















